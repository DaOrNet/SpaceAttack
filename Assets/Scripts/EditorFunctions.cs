﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class EditorFunctions : MonoBehaviour {
#if UNITY_EDITOR
	[MenuItem("Parser/ParceData")]
	public static void Parcer()
	{
		GameData.Instance = DataImporter.ParceAllData();
		
		SaveGameData(GameData.Instance);
	}
	public static void SaveGameData(GameData gd)
	{
		var bf = new BinaryFormatter();
		bf.AssemblyFormat=FormatterAssemblyStyle.Simple;
		using (var file = File.Create("Assets/Raw/gd.bytes"))
		{
			bf.Serialize(file, gd);
		}
		AssetDatabase.Refresh();
            
		var buildMap = new List<AssetBundleBuild>();
		var bundle = new AssetBundleBuild();
		bundle.assetBundleName = "gd.asset";

		bundle.assetNames = new[] { "Assets/Raw/gd.bytes" };
		buildMap.Add(bundle);
		var bundlesPath = Application.streamingAssetsPath + "/";

		BuildPipeline.BuildAssetBundles(bundlesPath, buildMap.ToArray(), BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget);
		
		Debug.LogError("Build Gamedata done");
	}
	#endif
}
