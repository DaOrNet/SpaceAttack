﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class ShipDataView : MonoBehaviour
{

	public Slider HpSlider;
	public Slider ShieldsSlider;

	public void Setup(Ship shipInfo)
	{
		HpSlider.maxValue = shipInfo.MaxHp;
		shipInfo.Hp.Subscribe(result => { HpSlider.value = result; });
		ShieldsSlider.maxValue = shipInfo.MaxShields;
		shipInfo.Shields.Subscribe(result => { ShieldsSlider.value = result; });
	}
}
