﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public enum WindowTypeEnum
{
    UpgradeWindow,
}
public class UIWindow : MonoBehaviour
{
    public WindowTypeEnum WindowType;

    public Button CloseButton;
    public Button BackgroundButton;
    public ConnectionsCollector Connections;
    public virtual void Show(object data)
    {
        if (Connections==null)
            Connections = new ConnectionsCollector();
        Connections.DisconnectAll();
        gameObject.SetActiveSafe(true);
        CloseButton.onClick.AddListener(Close);
        BackgroundButton.onClick.AddListener(Close);
        Connections.add = Disposable.Create(()=>
        {
            CloseButton.onClick.RemoveListener(Close);
        });
        Connections.add = Disposable.Create(()=>
        {
            BackgroundButton.onClick.RemoveListener(Close);
        });
    }

    public virtual void Close()
    {
        gameObject.SetActiveSafe(false);
        Connections.DisconnectAll();
    }
}
