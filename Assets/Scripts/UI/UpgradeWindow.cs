﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeWindow : UIWindow
{
    public GameObject ShipInfoPanel;
    public GameObject AbilityInfoPanel;

    public Button ShipComponentsSelector;
    public Button ShipWeaponsSelector;

    public Transform UpgradesInfoContainer;
    
    public UpgradeInfoPanel UpgradeInfoPrefab;

    public Image AbilityIcon;
    public Text AbilityInfo;
    public Text AbilityReloadInfo;
    public Text AbilityPriceInfo;
    public Button AbilityUpgradeButton;

    private Ship _playerShip;
    private LevelUpTarget _target;


    public override void Show(object data)
    {
        base.Show(data);
        ShipInfoPanel.SetActiveSafe(false);
        AbilityInfoPanel.SetActiveSafe(false);
        _target = data as LevelUpTarget;
        _playerShip = InstanceController.Instance.PlayerData;
        if (_target.UpgradeParts[0] is Ability)
        {
            AbilityInfoPanel.SetActiveSafe(true);
            var ability = (Ability) _target.UpgradeParts[0];
            BindAbilityView(ability);
        }
        else
        {
            ShipInfoPanel.SetActiveSafe(true);
            BindComponentsUpgradeView();
            Connections.add = Disposable.Create(ShipComponentsSelector.onClick.RemoveAllListeners);
            Connections.add = Disposable.Create(ShipWeaponsSelector.onClick.RemoveAllListeners);

            ShipComponentsSelector.onClick.AddListener(BindComponentsUpgradeView);
            ShipWeaponsSelector.onClick.AddListener(BindWeaponsUpgradeView);
        }
    }

    public void BindAbilityView(Ability ability)
    {
        var uiContoller = MainUIController.Instance;
        AbilityIcon.sprite = Resources.Load<Sprite>("Sprites/Abilities/"+ability.IconName);
        if (!UpgradeController.HasNextUpgrade(ability))
        {
            AbilityPriceInfo.text = "Max level";
        }
        else
        {
            AbilityPriceInfo.text = "Cost: " + UpgradeController.GetCost(ability);
            if (UpgradeController.CanUpgrade(ability))
            {
                //Выделить кнопочку
            }
        }
        AbilityReloadInfo.text = "Reload time : " + ability.ReloadTime + " seconds";
        IDisposable disp;
        Connections.add = disp = Disposable.Create(AbilityUpgradeButton.onClick.RemoveAllListeners);
        AbilityUpgradeButton.onClick.AddListener(() =>
        {
            if (!UpgradeController.CanUpgrade(ability))
                return;
            _target.UpgradeParts[0] = UpgradeController.UpgradeShipAbility(_playerShip,ability);
            var reloadImg = _target.transform.GetChild(0).GetComponent<Image>();
            reloadImg.SetActiveSafe(false);

            uiContoller.BindAbilityActivator(_target.GetComponent<Image>(), (Ability)_target.UpgradeParts[0], _playerShip);

            BindAbilityView((Ability)_target.UpgradeParts[0]);
            disp.Dispose();
        });
        
    }

    private void BindComponentsUpgradeView()
    {
        ShipComponentsSelector.GetComponent<Image>().color = Color.white;
        ShipWeaponsSelector.GetComponent<Image>().color = Color.gray;
        var allComponents = _target.UpgradeParts.OfType<ShipComponent>().ToList();
        for (int i = 0; i < UpgradesInfoContainer.childCount; i++)
        {
            Destroy(UpgradesInfoContainer.GetChild(0).gameObject);
        }
        UpgradesInfoContainer.DetachChildren();
        for (int i = 0; i < allComponents.Count; i++)
        {
            var currentComponent = allComponents[i];
            var info = Instantiate(UpgradeInfoPrefab, UpgradesInfoContainer);
            LoadInfoAboutComponent(info,currentComponent);
            if (!UpgradeController.HasNextUpgrade(currentComponent))
                return;
            
            Connections.add = Disposable.Create(info.UpgradeButton.onClick.RemoveAllListeners);
            info.UpgradeButton.onClick.AddListener(() =>
            {
                if (!UpgradeController.CanUpgrade(currentComponent))
                    return;
                currentComponent = UpgradeController.UpgradeShipComponent(_playerShip, currentComponent);
                LoadInfoAboutComponent(info, currentComponent);
            });
        }
    }

    private void LoadInfoAboutComponent(UpgradeInfoPanel panel, ShipComponent component)
    {
        var upgradeData = GameData.Instance.UpgradesData.FirstOrDefault(pair => pair.Key == component.NextLevelId);
        panel.HeaderText.text = component.Id;
        if (upgradeData.Equals(default(KeyValuePair<string, float>)))
        {
            panel.PriceText.text = "Max level";
            return;
        }
        panel.PriceText.text = "Cost: " + upgradeData.Value;
    }

    private void BindWeaponsUpgradeView()
    {
        ShipComponentsSelector.GetComponent<Image>().color = Color.gray;
        ShipWeaponsSelector.GetComponent<Image>().color = Color.white;
        var allWeapons = _target.UpgradeParts.OfType<Weapon>().ToList();
        for (int i = 0; i < UpgradesInfoContainer.childCount; i++)
        {
            Destroy(UpgradesInfoContainer.GetChild(0).gameObject);
        }
        UpgradesInfoContainer.DetachChildren();
        for (int i = 0; i < allWeapons.Count; i++)
        {
            var currentWeapon = allWeapons[i];
            var info = Instantiate(UpgradeInfoPrefab, UpgradesInfoContainer);
            
            LoadInfoAboutWeapon(info,currentWeapon);
            if (!UpgradeController.HasNextUpgrade(currentWeapon))
                return;
            
            Connections.add = Disposable.Create(info.UpgradeButton.onClick.RemoveAllListeners);
            info.UpgradeButton.onClick.AddListener(() =>
            {
                if (!UpgradeController.CanUpgrade(currentWeapon))
                    return;
                currentWeapon = UpgradeController.UpgradeShipWeapon(_playerShip, currentWeapon);
                MainUIController.Instance.WeaponInfo.InitializeShipInfo(_playerShip);
                LoadInfoAboutWeapon(info, currentWeapon);
            });
        }
    }

    private void LoadInfoAboutWeapon(UpgradeInfoPanel panel, Weapon component)
    {
        var upgradeData = GameData.Instance.UpgradesData.FirstOrDefault(pair => pair.Key == component.NextLevelId);
        panel.HeaderText.text = component.Id;
        if (upgradeData.Equals(default(KeyValuePair<string, float>)))
        {
            panel.PriceText.text = "Max level";
            return;
        }
        panel.PriceText.text = "Cost: " + upgradeData.Value;
    }
}
