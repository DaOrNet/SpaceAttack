﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUpTarget : MonoBehaviour,IUpgradeTarget
{

	private List<IUpgradable> _upgradeParts = new List<IUpgradable>();

	public List<IUpgradable> UpgradeParts
	{
		get { return _upgradeParts; }
	}
}
