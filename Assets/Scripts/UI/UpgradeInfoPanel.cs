﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeInfoPanel : MonoBehaviour
{
	public Image MainIcon;
	public Text DescriptionText;
	public Button UpgradeButton;
	public Text PriceText;
	public Text HeaderText;
}
