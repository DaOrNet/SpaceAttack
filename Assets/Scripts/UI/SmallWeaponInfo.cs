﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class SmallWeaponInfo : MonoBehaviour
{
	public Text WeaponId, WeaponStatus;
	public Image WeaponIcon;

	private ConnectionsCollector _collector = new ConnectionsCollector();
	
	public void InitializeShipInfo(Ship shipToShow)
	{
		_collector.DisconnectAll();
		_collector.add = shipToShow.SelectedWeapon.Subscribe(number =>
		{
			WeaponId.text = shipToShow.Weapons[number].Id;
			_collector.add = shipToShow.Weapons[number].SubscribeToStatus(newStatus => { WeaponStatus.text = newStatus; });
		});
	}
}
