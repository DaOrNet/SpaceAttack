﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowHelper : MonoBehaviour
{

	public Transform TargetToFollow;

	public MeshRenderer BackgroundMesh;

	public float Speed = 0.12f;
	
	private Transform _transform;
	void Start()
	{
		_transform = transform;
		
	}

	void FixedUpdate () {
		if (TargetToFollow != null)
		{
			Vector3 smoothedPosition = Vector2.Lerp(_transform.position, TargetToFollow.position, Speed);
			smoothedPosition.z = -100;
			_transform.position = smoothedPosition;
			BackgroundMesh.material.mainTextureOffset -= (Vector2)(_transform.position- TargetToFollow.position)* Speed / 300f;
		}
	}
}
