﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UniRx;
using UnityEngine;
[Serializable]
public class Asteroid:ParentHolder<AsteroidScript>,IDamagable
{
	public string Id;
	public int MaxHp;

	public float CollisionDamage;
	public float PartDamage;

	public float AsteroidScaleX;
	public float AsteroidScaleY;

	public string PrefabPath;

	[JsonIgnore] private float _hp;
	
	public ReactiveProperty<bool> Destroyed;

	public void Init()
	{
		_hp = MaxHp;
		Destroyed = new ReactiveProperty<bool>(false);
	}
	
	public void GetDamage(DamageTypeEnum damageType, float damage, MonoBehaviour sender)
	{
		_hp -= damage;
		if (_hp < 0)
		{
			GetCriticalDamage(sender);
		}
	}

	public void GetCriticalDamage(MonoBehaviour sender)
	{
		root.GetCriticalDamage(sender);
	}

	public float CheckDamagedStatus(MonoBehaviour sender)
	{
		throw new NotImplementedException();
	}
}
