﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputController : MonoBehaviour
{

	private KeyValuePair<int,IDisposable> _leftSubscription = new KeyValuePair<int, IDisposable>(-1,null);
	private KeyValuePair<int,IDisposable> _rightSubscription = new KeyValuePair<int, IDisposable>(-1,null);

	private MainUIController _uiController;

	private int _blockJoystickLayer;
	private void Start()
	{
		_uiController = MainUIController.Instance;
		_blockJoystickLayer = SortingLayer.GetLayerValueFromName("BlockJoysticks");
	}

	void Update () {
		var touches = GetTouches();
		var count = touches.Count;
		if ((count > 0))
		{
			for (int i = 0; i < count; i++)
			{
				var curTouch = touches[i];
				if (curTouch.phase == TouchPhase.Began)
				{
					List<RaycastResult> results = new List<RaycastResult>();
					var pointerEventData =
						new PointerEventData(_uiController.GameEventSystem) {position = curTouch.position};
					_uiController.UIRaycaster.Raycast(pointerEventData, results);
					
					if(results.Any(hit=>hit.sortingLayer==_blockJoystickLayer))
						continue;
					if (_leftSubscription.Key==-1 &&curTouch.position.x < Screen.width / 2.5f)
					{
						_leftSubscription = new KeyValuePair<int, IDisposable>(curTouch.fingerId,
							Disposable.Create(() =>
							{
								_uiController.JoystickControl.SetActiveSafe(false);
							}));
						
						_uiController.JoystickControl.transform.position = new Vector2(curTouch.position.x,curTouch.position.y);
						_uiController.JoystickControl.SetActiveSafe(true);
					}
					if (_rightSubscription.Key==-1 && curTouch.position.x > Screen.width - (Screen.width / 2.5f))
					{
						_rightSubscription = new KeyValuePair<int, IDisposable>(curTouch.fingerId,
							Disposable.Create(() =>
							{
								MainUIController.Instance.JoystickAttack.SetActiveSafe(false);
							}));
						_uiController.JoystickAttack.transform.position = new Vector2(curTouch.position.x,curTouch.position.y);
						_uiController.JoystickAttack.SetActiveSafe(true);
					}
				}
				if (curTouch.phase == TouchPhase.Moved)
				{
					if (_leftSubscription.Key == curTouch.fingerId)
					{
						_uiController.JoystickControl.ChangePosition(curTouch.position);
					}
					if (_rightSubscription.Key == curTouch.fingerId)
					{
						_uiController.JoystickAttack.ChangePosition(curTouch.position);
					}
					
				}
				if (curTouch.phase == TouchPhase.Ended)
				{
					if (_leftSubscription.Key == curTouch.fingerId)
					{
						_leftSubscription.Value.Dispose();
						_leftSubscription = new KeyValuePair<int, IDisposable>(-1, null);
						_uiController.JoystickControl.ResetPosition();
					}
					if (_rightSubscription.Key == curTouch.fingerId)
					{
						_rightSubscription.Value.Dispose();
						_rightSubscription = new KeyValuePair<int, IDisposable>(-1, null);
						_uiController.JoystickAttack.ResetPosition();
					}
				}
			}
		}
	}
	
	private static TouchCreator lastFakeTouch;
 
	public static List<Touch> GetTouches()
	{
		List<Touch> touches = new List<Touch>();
		touches.AddRange(Input.touches);
#if UNITY_EDITOR
		if(lastFakeTouch == null) lastFakeTouch = new TouchCreator();
		if(Input.GetMouseButtonDown(0))
		{
			lastFakeTouch.phase = TouchPhase.Began;
			lastFakeTouch.deltaPosition = new Vector2(0,0);
			lastFakeTouch.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			lastFakeTouch.fingerId = 0;
		}
		else if (Input.GetMouseButtonUp(0))
		{
			lastFakeTouch.phase = TouchPhase.Ended;
			Vector2 newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			lastFakeTouch.deltaPosition = newPosition - lastFakeTouch.position;
			lastFakeTouch.position = newPosition;
			lastFakeTouch.fingerId = 0;
		}
		else if (Input.GetMouseButton(0))
		{
			//lastFakeTouch.phase = TouchPhase.Moved;
			Vector2 newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			lastFakeTouch.deltaPosition = newPosition - lastFakeTouch.position;
			lastFakeTouch.phase = Math.Abs(lastFakeTouch.deltaPosition.magnitude) < 0.001f ? TouchPhase.Stationary : TouchPhase.Moved;
			lastFakeTouch.position = newPosition;
			lastFakeTouch.fingerId = 0;
		}
		else
		{
			lastFakeTouch = null;
		}
		if (lastFakeTouch != null) touches.Add(lastFakeTouch.Create());
#endif
            
 
		return touches;      
	}
}
 
public class TouchCreator
{
	static Dictionary<string, FieldInfo> fields;
 
	object touch;
 
	public float deltaTime { get { return ((Touch)touch).deltaTime; } set { fields["m_TimeDelta"].SetValue(touch, value); } }
	public int tapCount { get { return ((Touch)touch).tapCount; } set { fields["m_TapCount"].SetValue(touch, value); } }
	public TouchPhase phase { get { return ((Touch)touch).phase; } set { fields["m_Phase"].SetValue(touch, value); } }
	public Vector2 deltaPosition { get { return ((Touch)touch).deltaPosition; } set { fields["m_PositionDelta"].SetValue(touch, value); } }
	public int fingerId { get { return ((Touch)touch).fingerId; } set { fields["m_FingerId"].SetValue(touch, value); } }
	public Vector2 position { get { return ((Touch)touch).position; } set { fields["m_Position"].SetValue(touch, value); } }
	public Vector2 rawPosition { get { return ((Touch)touch).rawPosition; } set { fields["m_RawPosition"].SetValue(touch, value); } }
 
	public Touch Create()
	{
		return (Touch)touch;
	}
     
	public TouchCreator()
	{
		touch = new Touch();
	}
 
	static TouchCreator()
	{
		fields = new Dictionary<string, FieldInfo>();
		foreach(var f in typeof(Touch).GetFields(BindingFlags.Instance | BindingFlags.NonPublic))
		{
			fields.Add(f.Name, f);
		}
	}
}
