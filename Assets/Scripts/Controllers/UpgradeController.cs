﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

public static class UpgradeController {

	private static List<KeyValuePair<IUpgradeTarget, Transform>> _upgradeObjects;
	static UpgradeController()
	{
		_upgradeObjects = new List<KeyValuePair<IUpgradeTarget, Transform>>();
		InstanceController.Instance.ScrapCollected.Subscribe(scrapVal =>
		{
			var canUpgrade = _upgradeObjects.Any(upgr => upgr.Key.UpgradeParts.Any(upgradable =>
				{
					var upgrade =
						GameData.Instance.UpgradesData.FirstOrDefault(keyval => keyval.Key == upgradable.NextLevelKey);
					return !upgrade.Equals(default(KeyValuePair<string, float>)) && scrapVal > upgrade.Value;
				}
			));
			GameController.Instance.UIController.UpgradeButtonChangeState(canUpgrade);
		});
	}
	
	public static bool CanUpgrade(IUpgradable component)
	{
		if (string.IsNullOrEmpty(component.NextLevelKey))
			return false;
		var upgradeData = GameData.Instance.UpgradesData.FirstOrDefault(pair => pair.Key == component.NextLevelKey);
		if (upgradeData.Value > InstanceController.Instance.ScrapCollected.Value)
		{
			return false;
		}
		return true;
	}
	
	public static bool HasNextUpgrade(IUpgradable component)
	{ 
		return !string.IsNullOrEmpty(component.NextLevelKey);
	}
	
	public static float GetCost(IUpgradable component)
	{
		var upgradeData = GameData.Instance.UpgradesData.FirstOrDefault(pair => pair.Key == component.NextLevelKey);
		return upgradeData.Value;
	}

	public static ShipComponent UpgradeShipComponent(Ship playerShip, IUpgradable component)
	{
		var upgradeData = GameData.Instance.UpgradesData.FirstOrDefault(pair => pair.Key == component.NextLevelKey);
		for (int j = 0; j < playerShip.ShipComponents.Count; j++)
		{
			if (playerShip.ShipComponents[j] == component)
			{
				var workingComponent = playerShip.root.WorkingComponents.First(data=>data.Key==component);
				playerShip.root.WorkingComponents.Remove(workingComponent);
				workingComponent.Value.Dispose();
				playerShip.ShipComponents[j] = GameData.Instance.ShipComponentsData.First(pair => pair.Key == component.NextLevelKey).Value
					.CloneByJson();
				playerShip.root.StartComponentWork(playerShip.ShipComponents[j]);
				InstanceController.Instance.ScrapCollected.Value -= upgradeData.Value;     
				return playerShip.ShipComponents[j];
			}
		}

		return null;
	}
	
	public static Ability UpgradeShipAbility(Ship playerShip, IUpgradable component)
	{
		var upgradeData = GameData.Instance.UpgradesData.FirstOrDefault(pair => pair.Key == component.NextLevelKey);
		for (int j = 0; j < playerShip.Abilities.Count; j++)
		{
			if (playerShip.Abilities[j] == component)
			{
				playerShip.Abilities[j] = GameData.Instance.Abilities.First(pair => pair.Key == component.NextLevelKey).Value
					.CloneByJson();
				InstanceController.Instance.ScrapCollected.Value -= upgradeData.Value;
				return playerShip.Abilities[j];
			}
		}

		return null;
	}
	
	public static Weapon UpgradeShipWeapon(Ship playerShip, IUpgradable component)
	{
		var upgradeData = GameData.Instance.UpgradesData.FirstOrDefault(pair => pair.Key == component.NextLevelKey);
		for (int j = 0; j < playerShip.Weapons.Count; j++)
		{
			if (playerShip.Weapons[j] == component)
			{
				playerShip.Weapons[j].ToSleep();
				playerShip.Weapons[j] = GameData.Instance.WeaponsData.First(pair => pair.Key == component.NextLevelKey).Value
					.CloneByJson();
				var currentWeapon = playerShip.Weapons[j];
				currentWeapon.root = playerShip;
				currentWeapon.Init();
				InstanceController.Instance.ScrapCollected.Value -= upgradeData.Value;
				return currentWeapon;
			}
		}

		return null;
	}
	
	public static void RegisterUpgradeTargets(IUpgradeTarget targetsData, Transform parent)
	{
		_upgradeObjects.Add(new KeyValuePair<IUpgradeTarget, Transform>(targetsData,parent));
	}

	
}
