﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;
using Newtonsoft.Json;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

public class ShipController : MonoBehaviour,IDamagable,IPoolable,IScrapCollector
{
	[JsonIgnore][NonSerialized]
	public ConnectionsCollector Collector;
	public Ship Data;
	public Transform CashedTransform;
	private Rigidbody2D _body;
	public ReactiveProperty<bool> AI;
	public Transform WeaponsHolder;
	public List<KeyValuePair<ShipComponent, IDisposable>> WorkingComponents;

	public void Init()
	{
		if (Collector == null)
			Collector = new ConnectionsCollector();
		CashedTransform = transform;
		_body = GetComponent<Rigidbody2D>();
		AI = new ReactiveProperty<bool>(false);
		Coroutine coroutine = null;
		Collector.add = AI.Subscribe(val =>
		{
			if (val)
			{
				coroutine = StartCoroutine(AILogic());
			}
			else
			{
				if (coroutine!=null)
					StopCoroutine(coroutine);
			}
		});
		Collector.add = Data.FireOn.Subscribe(val => {
			if (val)
			{
				Data.Fire();
			}
		});
	}

	public void StartComponentWork(ShipComponent component)
	{
		if (WorkingComponents == null)
			WorkingComponents = new List<KeyValuePair<ShipComponent, IDisposable>>();
		if (WorkingComponents.Any(data=>data.Key==component))return;
		component.root = Data;
		StartCoroutine(component.InternalWork());
		WorkingComponents.Add(new KeyValuePair<ShipComponent, IDisposable>(component,Disposable.Create(()=>StopCoroutine(component.InternalWork()))));
	}
	private void OnEnable()
	{
		StartCoroutine(Data.InternalWork());
		Data.ShipComponents.ForEach(StartComponentWork);
	}
	private void OnDisable()
	{
		if (WorkingComponents!=null)
			WorkingComponents.ForEach(data => data.Value.Dispose());
		StopAllCoroutines();
		Collector.DisconnectAll();
		Reuse();
	}
	
	private IEnumerator Process()
	{
		yield break;
	}
	
	private IEnumerator AILogic()
	{
		
		while (true)
		{
			var targetedPlayerPos = InstanceController.Instance.Player.position;
			float currentRot = Mathf.Atan2(CashedTransform.up.y, CashedTransform.up.x) * Mathf.Rad2Deg;
			float neededRot = Mathf.Atan2(targetedPlayerPos.y - CashedTransform.position.y,targetedPlayerPos.x - CashedTransform.position.x) * Mathf.Rad2Deg;
			transform.rotation =
				Quaternion.AngleAxis(
					Mathf.LerpAngle(currentRot, neededRot, Data.Mobility * Time.deltaTime) - 90,
					Vector3.forward);
			var diff = Math.Abs(neededRot - currentRot);
			diff = diff > 180 ? 360 - diff : diff;
			var speedPart = diff / 20f;
			speedPart = speedPart < 1 ? 1 : speedPart;
			_body.AddForceSave(CashedTransform.up * Vector2.Distance(Vector2.one, Vector2.zero) * Time.deltaTime *
			               (Data.EnginePower / speedPart),3f);
			if (Vector2.Distance(targetedPlayerPos, (Vector2) CashedTransform.position) < 10)
			{
				Data.WeaponRotation = neededRot;
				Data.FireOn.Value = true;
			}
			else
			{
				Data.FireOn.Value = false;
			}
			yield return new WaitForFixedUpdate();
		}
	}


	public void GetDamage(DamageTypeEnum damageType, float damage, MonoBehaviour sender)
	{
		Data.GetDamage(damageType,damage,sender);
	}

	public void GetCriticalDamage(MonoBehaviour sender)
	{
		for (int i = 0; i < 3; i++)
		{
			var explosion = InstanceController.Instance.LoadEffectFromCashe(EffectsEnum.Explosion);
			explosion.transform.position =
				transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
			explosion.SetActiveSafe(true);
		}
		gameObject.SetActiveSafe(false);
	}

	public float CheckDamagedStatus(MonoBehaviour sender)
	{
		throw new System.NotImplementedException();
	}

	public Action OnFree { get; set; }
	public IEnumerator WaitForReuse(float delay)
	{
		throw new NotImplementedException();
	}

	public void Reuse()
	{
		AI.Value = false;
		OnFree.Invoke();
	}

	public bool CanCollectScrap = false;
	public bool Collectable
	{
		get { return CanCollectScrap; }
	}
	public void CollectScrap(Scrap scrap)
	{
		InstanceController.Instance.ScrapCollected.Value += scrap.ScrapCount;
	}
}
