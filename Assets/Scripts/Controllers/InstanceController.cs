﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using Random = UnityEngine.Random;

public class InstanceController : MonoBehaviour
{

	public static InstanceController Instance;

	public MinimapController Minimap;

	public Transform EnemiesHolder;
	public Transform AsteroidsHolder;
	
	public Transform CasheNode;
	private List<KeyValuePair<string, GameObject>> _cashedObjects;

	public List<GameObject> ExplosionsCashe;
	public List<KeyValuePair<string,ShipController>> ShipsCashe;
	public List<KeyValuePair<string,AsteroidScript>> AsteroidsCashe;
	public List<KeyValuePair<string,ScrapScript>> ScrapsCashe;
	
	public ManyObjectsPool<ShipController> ShipsPools;
	
	public ManyObjectsPool<AsteroidScript> AsteroidsPools;
	
	public ManyObjectsPool<ScrapScript> ScrapsPools;

	public ObservableCollection<KeyValuePair<int,ShipController>> Enemies;
	public ObservableCollection<KeyValuePair<int,AsteroidScript>> Asteroids;

	public Transform Player;
	public Ship PlayerData;

	public ShipDataView ShipBars;
	
	public float RegenerateRange = 19;
	public float CreatingRange = 12;

	public int EnemyShipsCount = 3;
	public int AsteroidsCount = 1;

	public ReactiveProperty<float> ScrapCollected;
	
	public bool ShowDebugVisualInfo = true;
	public bool ShowRemoveRange = true;
	public bool ShowCreatingRange = true;

	public bool Immortal;

	public void Awake()
	{
		Instance = this;
	}
	
	public void StartFight()
	{
		if (ScrapCollected!=null)
			ScrapCollected.Dispose();
		_cashedObjects = new List<KeyValuePair<string, GameObject>>();
		ScrapCollected = new ReactiveProperty<float>();
		ExplosionsCashe=new List<GameObject>();
		Enemies = new ObservableCollection<KeyValuePair<int, ShipController>>();
		Asteroids = new ObservableCollection<KeyValuePair<int, AsteroidScript>>();
		ShipsCashe = new List<KeyValuePair<string,ShipController>>();
		AsteroidsCashe = new List<KeyValuePair<string, AsteroidScript>>();
		LoadToCasheAndPools();
		InstantiateCurrentShip();
		InstantiateEnemies();
		InstantiateAsteroids();
		StartCoroutine(RegenerateAround());
		Minimap.Init();
	}

	private void InstantiateCurrentShip()
	{
		//TODO: Коллайдеры
		var primalBody = PrimalBody.Instance;
		var ship = GetOrCreateShip(primalBody.OpenedShips[primalBody.SelectedShip]);
		Player = ship.transform;
		ship.gameObject.AddComponent<PlayerController>();
		Camera.main.GetComponent<CameraFollowHelper>().TargetToFollow = ship.transform;
		ship.gameObject.SetActiveSafe(true);
		ShipBars.Setup(ship.Data);
		PlayerData = ship.Data;
	}

	private void InstantiateEnemies()
	{
		for (int i = 0; i < 0; i++)
		{
			var ship = GetOrCreateShip(2);
			Enemies.Add(new KeyValuePair<int, ShipController>(2,ship));
			ship.Data.Alive.Subscribe(val =>
			{
				if (!val)
				{
					Enemies.RemoveAll(pair => pair.Value == ship);
				}
			});
			ship.transform.position=new Vector3(-3.9f+Random.Range(-10,5),3.8f+Random.Range(-10,5),0);
			
			ship.gameObject.SetActiveSafe(true);
		}
	}

	private void InstantiateAsteroids()
	{
		
		for (int i = 0; i < 0; i++)
		{
			int id = Random.Range(0,2);
			var asteroid = GetOrCreateAsteroid(id);
			Asteroids.Add(new KeyValuePair<int, AsteroidScript>(id,asteroid));
			asteroid.Data.Init();
			asteroid.Collector.add = asteroid.Data.Destroyed.Subscribe(val =>
			{
				if (val)
				{
					Enemies.RemoveAll(pair => pair.Value == asteroid);
				}
			});
			var euler = asteroid.SpriteTransform.eulerAngles;
			euler.z = Random.Range(0.0f, 360.0f);
			asteroid.SpriteTransform.eulerAngles = euler;
			asteroid.gameObject.SetActiveSafe(true);
		}
	}

	private ShipController GetOrCreateShip(int id)
	{
		return GetOrCreateShip(GameData.Instance.AllShips[id]);
	}

	private ShipController GetOrCreateShip(Ship target)
	{
		var ship = ShipsPools.GetUnusedObject(target.ShipPrefabPath);
		var shipScale = target.ShipScale;
		ship.transform.localScale =new Vector3(shipScale,shipScale,1);
		var shipController = ship.GetComponent<ShipController>();
		shipController.Data = target.CloneByJson();
		shipController.Data.root = shipController;
		shipController.Data.Init();
		ship.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(
			"Sprites/SpaceShips/" + target.ShipImagePath);
		return ship;
	}
	
	private AsteroidScript GetOrCreateAsteroid(int id)
	{
		var asteroid = AsteroidsPools.GetUnusedObject(GameData.Instance.Asteroids[id].PrefabPath);
		var asteroidScaleX =GameData.Instance.Asteroids[id].AsteroidScaleX;
		var asteroidScaleY =GameData.Instance.Asteroids[id].AsteroidScaleY;
		asteroid.transform.localScale =new Vector3(asteroidScaleX,asteroidScaleY,1);
		var asteriodScript = asteroid.GetComponent<AsteroidScript>();
		asteriodScript.Data = GameData.Instance.Asteroids[id].CloneByJson();
		asteriodScript.Init(false);
		asteriodScript.Data.Init();
//		Debug.Log("Sprites/Asteroids/" + GameData.Instance.Asteroids[id].Id);
		asteriodScript.SpriteTransform.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(
			"Sprites/Asteroids/" + GameData.Instance.Asteroids[id].Id);
		return asteriodScript;
	}

	private void LoadToCasheAndPools()
	{
		ExplosionsCashe.Add(Instantiate(Resources.Load<GameObject>("Prefab/Explosions/Explosion"), CasheNode));
		var poolFuncs = new List<KeyValuePair<string, Func<ShipController>>>();
		var specials = new List<KeyValuePair<string, bool>>();
		for (int i = 0; i < GameData.Instance.AllShips.Length; i++)
		{
			if (ShipsCashe.All(data => data.Key != GameData.Instance.AllShips[i].ShipPrefabPath))
			{
				var ship = Resources.Load<ShipController>(GameData.Instance.AllShips[i].ShipPrefabPath);
				var keyval = new KeyValuePair<string, ShipController>(GameData.Instance.AllShips[i].ShipPrefabPath,
					Instantiate(ship, CasheNode));
				ShipsCashe.Add(keyval);
				var temp = i;
				poolFuncs.Add(new KeyValuePair<string, Func<ShipController>>(GameData.Instance.AllShips[temp].ShipPrefabPath,
					()=>Instantiate(ShipsCashe.First(casheData=> casheData.Key==GameData.Instance.AllShips[temp].ShipPrefabPath).Value,EnemiesHolder)));
				specials.Add(new KeyValuePair<string, bool>(GameData.Instance.AllShips[temp].ShipPrefabPath,false));
			}
			GameData.Instance.AllShips[i].Weapons.ForEach(LoadWeaponCashe);
		}
		ShipsPools = new ManyObjectsPool<ShipController>(poolFuncs,specials);
		var asteroidsPoolFuncs = new List<KeyValuePair<string, Func<AsteroidScript>>>();
		specials = new List<KeyValuePair<string, bool>>();
		for (int i = 0; i < GameData.Instance.Asteroids.Length; i++)
		{
			if (AsteroidsCashe.All(data => data.Key != GameData.Instance.Asteroids[i].PrefabPath))
			{
				var asteroid = Resources.Load<AsteroidScript>(GameData.Instance.Asteroids[i].PrefabPath);
				var keyval = new KeyValuePair<string, AsteroidScript>(GameData.Instance.Asteroids[i].PrefabPath,
					Instantiate(asteroid, CasheNode));
				AsteroidsCashe.Add(keyval);
				var temp = i;
				asteroidsPoolFuncs.Add(new KeyValuePair<string, Func<AsteroidScript>>(GameData.Instance.Asteroids[temp].PrefabPath,
					()=>Instantiate(AsteroidsCashe.First(casheData=>casheData.Key==GameData.Instance.Asteroids[temp].PrefabPath).Value,AsteroidsHolder)));
				specials.Add(new KeyValuePair<string, bool>(GameData.Instance.Asteroids[temp].PrefabPath,false));
			}
		}
		var asteroidParent = Instantiate(Resources.Load<AsteroidScript>("Prefab/Asteroids/AsteroidPart"), CasheNode);
		specials.Add(new KeyValuePair<string, bool>("part",true));
		asteroidsPoolFuncs.Add(new KeyValuePair<string, Func<AsteroidScript>>("part",()=>Instantiate(asteroidParent,AsteroidsHolder)));
		
		AsteroidsPools = new ManyObjectsPool<AsteroidScript>(asteroidsPoolFuncs,specials);
		ScrapsCashe = new List<KeyValuePair<string, ScrapScript>>();
		var scrapPoolFuncs = new List<KeyValuePair<string, Func<ScrapScript>>>();
		specials = new List<KeyValuePair<string, bool>>();
		for (int i = 0; i < GameData.Instance.ScrapsData.Count; i++)
		{
			if (ScrapsCashe.All(data => data.Key != GameData.Instance.ScrapsData[i].Value.PrefabPath))
			{
				var scrap = Resources.Load<ScrapScript>(GameData.Instance.ScrapsData[i].Value.PrefabPath);
				var keyval = new KeyValuePair<string, ScrapScript>(GameData.Instance.ScrapsData[i].Value.PrefabPath,
					Instantiate(scrap, CasheNode));
				ScrapsCashe.Add(keyval);
				var temp = i;
				scrapPoolFuncs.Add(new KeyValuePair<string, Func<ScrapScript>>(GameData.Instance.ScrapsData[temp].Value.PrefabPath,
					()=>
					{
						var obj = Instantiate(ScrapsCashe.First(casheData =>
							casheData.Key == GameData.Instance.ScrapsData[temp].Value.PrefabPath).Value);
						return obj;
					}));
				specials.Add(new KeyValuePair<string, bool>(GameData.Instance.ScrapsData[temp].Value.PrefabPath,false));
			}
		}
		ScrapsPools = new ManyObjectsPool<ScrapScript>(scrapPoolFuncs,specials);
		ScrapsPools.ObjectsLimit = 80;
	}

	private void LoadWeaponCashe(Weapon currentWeapon)
	{
		if (string.IsNullOrEmpty(currentWeapon.WeaponPrefabToLoad) ||
		    _cashedObjects.Any(pair => pair.Key == currentWeapon.WeaponPrefabToLoad))
			return;
		_cashedObjects.Add(new KeyValuePair<string, GameObject>(currentWeapon.WeaponPrefabToLoad,
			Instantiate(Resources.Load<GameObject>(currentWeapon.WeaponPrefabToLoad), CasheNode)));
		if (!string.IsNullOrEmpty(currentWeapon.NextLevelKey))
			LoadWeaponCashe(GameData.Instance.WeaponsData.First(pair => pair.Key == currentWeapon.NextLevelKey).Value);
	}

	public GameObject GetFromCashe(string id)
	{
		return _cashedObjects.FirstOrDefault(pair => pair.Key == id).Value;
	}
	
	public GameObject LoadEffectFromCashe(EffectsEnum effectType)
	{
		switch (effectType)
		{
			case EffectsEnum.Explosion:
			{
				return Instantiate(ExplosionsCashe[Random.Range(0,ExplosionsCashe.Count)]);
			}
		}

		return null;
	}
	
	private IEnumerator RegenerateAround()
	{
		while (true)
		{
			yield return new WaitForSeconds(1f);
			var playerPos = Player.position;
			var enemies = ShipsPools.GetAllObjectsInUse();
			foreach (var enemy in enemies)
			{
				var enemyPos = enemy.CashedTransform.position;
				var distance = Vector2.Distance(enemyPos, playerPos);
				if (distance > RegenerateRange)
				{
					enemy.SetActiveSafe(false);
					Enemies.RemoveAll(pair => pair.Value == enemy);
				}
			}
			var asteroids = AsteroidsPools.GetAllObjectsInUse();
			foreach (var aster in asteroids)
			{
				var asterPos = aster.ContainerTransform.position;
				var distance = Vector2.Distance(asterPos, playerPos);
				if (distance > RegenerateRange)
				{
					aster.SetActiveSafe(false);
					Asteroids.RemoveAll(pair => pair.Value == aster);
				}
			}

			if (enemies.Count < EnemyShipsCount)// TODO: генерация кол-ва и типов противников по сложности
			{
				for (int i = enemies.Count; i < EnemyShipsCount; i++)
				{
					var ship = GetOrCreateShip(2);
					ship.gameObject.SetActiveSafe(true);
					ship.Init();
					var radius = Random.Range(CreatingRange,RegenerateRange);
					var x = Random.Range(-1f, 1f) * radius;
					var y = Mathf.Sqrt(Mathf.Pow(radius, 2) - Mathf.Pow(x, 2)) * ((Random.Range(0f,1f)>0.5f)?1:-1);
					ship.CashedTransform.position=new Vector3(playerPos.x+x,playerPos.y+y,0);
					var euler = ship.CashedTransform.eulerAngles;
					euler.z = Random.Range(0.0f, 360.0f);
					ship.CashedTransform.eulerAngles = euler;
					Enemies.Add(new KeyValuePair<int, ShipController>(2,ship));
					ship.Data.Alive.Subscribe(val =>
					{
						if (!val)
						{
							Enemies.RemoveAll(pair => pair.Value == ship);
							GenerateReward(ship);
						}
					});
					
					
					ship.AI.Value = true;
					yield return new WaitForEndOfFrame();
				}
			}
			
			if (asteroids.Count < AsteroidsCount)// TODO: генерация кол-ва и типов противников по сложности
			{
				for (int i = asteroids.Count; i < AsteroidsCount; i++)
				{
					int id = Random.Range(0,2);
					var asteroid = GetOrCreateAsteroid(id);
					var radius = Random.Range(CreatingRange,RegenerateRange);
					var x = Random.Range(-1f, 1f) * radius;
					var y = Mathf.Sqrt(Mathf.Pow(radius, 2) - Mathf.Pow(x, 2)) * ((Random.Range(0f,1f)>0.5f)?1:-1);
					asteroid.ContainerTransform.position=new Vector3(playerPos.x+x,playerPos.y+y,0);
					Asteroids.Add(new KeyValuePair<int, AsteroidScript>(id,asteroid));
					asteroid.Collector.add = asteroid.Data.Destroyed.Subscribe(val =>
					{
						if (val)
						{
							Asteroids.RemoveAll(pair => pair.Value == asteroid);
						}
					});
					
					var euler = asteroid.SpriteTransform.eulerAngles;
					euler.z = Random.Range(0.0f, 360.0f);
					asteroid.SpriteTransform.eulerAngles = euler;
					asteroid.SetActiveSafe(true);
					yield return new WaitForEndOfFrame();
				}
			}
		}
	}

	private void GenerateReward(ShipController ship)
	{
		var startPos = ship.CashedTransform.position;
		var deathResources = ship.Data.Worth;
		var minScrap = GameData.Instance.MinScrapValue;
		var maxPacks = deathResources / minScrap;
		var packsCount = Random.Range(1, maxPacks>GameData.Instance.MaxScrapPacks?GameData.Instance.MaxScrapPacks:(int)maxPacks);
		for (int i = 0; i <= packsCount; i++)
		{
			var packValue = Random.Range(minScrap, deathResources - minScrap * (packsCount - (i + 1)));
			var scrapArray = GameData.Instance.ScrapsData
				.Where(pair => pair.Value.MinScrapCount <= packValue && pair.Value.MaxScrapCount >= packValue).ToArray();
			var scrapData = scrapArray.ElementAt(Random.Range(0, scrapArray.Length)).Value;
			var scrapView = ScrapsPools.GetUnusedObject(scrapData.PrefabPath);
			scrapView.Data = scrapData.CloneByJson();
			scrapView.Init(packValue);
			scrapView.SetActiveSafe(true);
			scrapView.StartCoroutine(scrapView.WaitForReuse(5f));
			scrapView.transform.position = startPos;
			scrapView.CashedRigidbody2D.AddForceSave(new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * Random.Range(0f, 10f),3f);
			deathResources -= packValue;
			
		}
	}

	private void OnDrawGizmos()
	{
		if (ShowDebugVisualInfo && Player!=null)
		{
			if (ShowRemoveRange)
			{
#if UNITY_EDITOR
				Handles.color = Color.red;
				Handles.DrawWireDisc(Player.position
					, Player.forward   
					, RegenerateRange);
#endif
			}

			if (ShowCreatingRange)
			{
#if UNITY_EDITOR
				Handles.color = Color.green;
				Handles.DrawWireDisc(Player.position
					, Player.forward   
					, CreatingRange);
#endif
			}
		}
        
	}
}

public enum EffectsEnum
{
	Explosion
}