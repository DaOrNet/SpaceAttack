﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private IDisposable _attackJoystick;
    private IDisposable _movementJoystick;

    private Vector2 _currentMoveDirection;
    private Vector2 _currentAttackDirection;

    private Rigidbody2D _body;
    private Transform _transform;

    private ShipController _underControl;

    //Граница между поворотом и началом движения
    [Range(0, 1)] public float RotationLimit=0.5f;

    private void Start()
    {
        _transform = transform;
        _body = GetComponent<Rigidbody2D>();
        _underControl = gameObject.GetComponent<ShipController>();
        _underControl.Init();
        _underControl.CanCollectScrap = true;
        _underControl.AI.Value = false;

        _movementJoystick =
            GameController.Streams.MovementJoystick.Subscribe(prop =>
                _currentMoveDirection = prop);
        _attackJoystick =
            GameController.Streams.AttackJoystick.Subscribe(prop =>
            {
                if (prop != Vector2.zero)
                {
                    _underControl.Data.WeaponRotation =  Mathf.Atan2(prop.y, prop.x) * Mathf.Rad2Deg;
                    _underControl.Data.FireOn.Value = true;
                }
                else
                {
                    _underControl.Data.FireOn.Value = false;
                }
            });
        if (InstanceController.Instance.Immortal)
        {
            _underControl.Data.Hp.Value = int.MaxValue;
        }
    }
    
    private void FixedUpdate()
    {
        if (_currentMoveDirection != Vector2.zero)
        {
            if (Vector2.Distance(_currentMoveDirection, Vector2.zero) > RotationLimit)
                _body.AddForceSave(
                    (_transform.up * Vector2.Distance(_currentMoveDirection, Vector2.zero) / RotationLimit) *
                    _underControl.Data.EnginePower * Time.deltaTime, 3f);
            float currentRot = Mathf.Atan2(_transform.up.y, _transform.up.x) * Mathf.Rad2Deg;
            float neededRot = Mathf.Atan2(_currentMoveDirection.y, _currentMoveDirection.x) * Mathf.Rad2Deg;
            if (Mathf.Abs(Mathf.Abs(currentRot) - Mathf.Abs(neededRot)) > 0.2)
            {
                //Постепенный поворот корабля, докрутить визуалку надо
                transform.rotation =
                    Quaternion.AngleAxis(
                        Mathf.LerpAngle(currentRot, neededRot, _underControl.Data.Mobility * Time.deltaTime) - 90,
                        Vector3.forward);
            }
        }
    }

    //TODO: Возможно в будущем стоит добавить вожножность автоуправления или абилки перехвата управления, но это в дальний ящик
    private void OnDisable()
    {
        _movementJoystick.Dispose();
        _attackJoystick.Dispose();
    }
}