﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoystickController : MonoBehaviour
{
    public Transform JoystickHandle;

    public JoystickTypeEnum JoystickType;

    public float MaxDistance;

    private Transform _transform;
    private Vector2 _zeroPos;

    private void Awake()
    {
        if (_transform == null)
        {
            _transform = transform;
            MaxDistance = MaxDistance * ((Screen.height / 800f + Screen.width / 1280f) / 2);
        }
    }

    private void OnEnable()
    {
        _zeroPos = transform.position;
    }

    public void ChangePosition(Vector2 newPosition)
    {
        Vector2 currentPos = newPosition;
        JoystickHandle.position = new Vector3(currentPos.x, currentPos.y, 0);
        if (Vector2.Distance(_zeroPos, currentPos) > MaxDistance)
        {
            //В случае выхода пальца за границы джойстика,
            //поставить изображение в максимальную точку по направлению пальца
            var deltaX = currentPos.x - _zeroPos.x;
            var deltaY = currentPos.y - _zeroPos.y;
            var thetaRadians = Mathf.Atan2(deltaY, deltaX);
            var sin = Mathf.Sin(thetaRadians);
            var cos = Mathf.Cos(thetaRadians);
            JoystickHandle.position = new Vector3(_zeroPos.x + cos * MaxDistance, _zeroPos.y + sin * MaxDistance, 0);
            if (JoystickType == JoystickTypeEnum.Control)
            {
                GameController.Streams.MovementJoystick.Value = new Vector2(cos, sin);
            }
            else
            {
                GameController.Streams.AttackJoystick.Value = new Vector2(cos, sin);
            }
        }
        else
        {
            JoystickHandle.position = new Vector3(currentPos.x, currentPos.y, 0);
            if (JoystickType == JoystickTypeEnum.Control)
            {
                GameController.Streams.MovementJoystick.Value =
                    new Vector2((currentPos.x-_zeroPos.x) / MaxDistance, (currentPos.y-_zeroPos.y)  / MaxDistance);
            }
            else
            {
                GameController.Streams.AttackJoystick.Value =
                    new Vector2((currentPos.x-_zeroPos.x)  / MaxDistance, (currentPos.y-_zeroPos.y) / MaxDistance);
            }
        }
    }

    public void ResetPosition()
    {
        //Возвращение к изначальной позиции
        JoystickHandle.transform.position = _zeroPos;
        if (JoystickType == JoystickTypeEnum.Control)
        {
            GameController.Streams.MovementJoystick.Value =new Vector2(0,0);
        }
        else
        {
            GameController.Streams.AttackJoystick.Value =new Vector2(0,0);
        }
        
    }
}