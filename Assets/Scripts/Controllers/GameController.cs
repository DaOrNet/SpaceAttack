﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class GameController : MonoBehaviour
{

	public static DataController Streams;

	public static GameController Instance;

	public MainUIController UIController;

	public InstanceController Ic;

	private void Awake()
	{	
		Init();
	}

	public void Start()
	{
		Instance = this;
		Ic = InstanceController.Instance;
		_timers = new List<Tuple<ReactiveProperty<float>, Action>>();
		Ic.StartFight();
		UIController.Init();
		UIController.InitAbilities(Ic.PlayerData);
	}
	
	private void Init()
	{
		Streams = new DataController();
		GameData.Instance = GameData.LoadGameData();
		PrimalBody.Instance.WakeUp();
	}

	public void Update()
	{
		for (int i = 0; i < _timers.Count; i++)
		{
			var tuple = _timers[i];
			tuple.Item1.Value -= Time.deltaTime;
			if (tuple.Item1.Value < double.Epsilon)
			{
				tuple.Item2.Invoke();
				_timers.Remove(tuple);
				i--;
			}
		}
	}

	private List<Tuple<ReactiveProperty<float>, Action>> _timers;
	public IDisposable SetTimer(Action act,ReactiveProperty<float> time)
	{
		var tuple = new Tuple<ReactiveProperty<float>, Action>(time,act);
		_timers.Add(tuple);
		return Disposable.Create(() =>
		{
			if (_timers.Contains(tuple))
				_timers.Remove(tuple);
		});
	}
}
