﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class MinimapController : MonoBehaviour
{

	public Transform PlayerMiniIcon;
	public Transform ObjectsHolder;

	private ObservableCollection<Tuple<string, MinimapObjectView, Transform>> _slowObservedObjects;
	private ObservableCollection<Tuple<string, MinimapObjectView, Transform>> _fastObservedObjects;

	public ManyObjectsPool<MinimapObjectView> TargetsPool;

	public ReactiveProperty<float> Scale = new ReactiveProperty<float>(1);
	public float MaxPixelsMinimap = 100;
	public float MaxDistanceVision = 8;

	private float coeficiente;
	
	public InstanceController Ic;

	private void Start()
	{
		Ic = InstanceController.Instance;
		
		
		var poolFuncs = new List<KeyValuePair<string, Func<MinimapObjectView>>>();
		var specials = new List<KeyValuePair<string, bool>>();

		var keyval = new KeyValuePair<string, MinimapObjectView>("enemy",
			Instantiate(Resources.Load<MinimapObjectView>("Prefab/Minimap/EnemyMiniIcon"), Ic.CasheNode));
		poolFuncs.Add(new KeyValuePair<string, Func<MinimapObjectView>>(keyval.Key,
			() =>
			{
				var view = Instantiate(keyval.Value, ObjectsHolder);
				view.Init();
				return view;
			}));
		specials.Add(new KeyValuePair<string, bool>(keyval.Key, false));
		var seckeyval = new KeyValuePair<string, MinimapObjectView>("asteroid",
			Instantiate(Resources.Load<MinimapObjectView>("Prefab/Minimap/AsteroidMiniIcon"), Ic.CasheNode));
		poolFuncs.Add(new KeyValuePair<string, Func<MinimapObjectView>>(seckeyval.Key,
			() =>
			{
				var view = Instantiate(seckeyval.Value, ObjectsHolder);
				view.Init();
				return view;
			}));
		specials.Add(new KeyValuePair<string, bool>(seckeyval.Key, false));
			
		
		TargetsPool = new ManyObjectsPool<MinimapObjectView>(poolFuncs,specials);

		Scale.Subscribe(val => { coeficiente = MaxDistanceVision * val / MaxPixelsMinimap; });
	}

	public void Init()
	{
		_slowObservedObjects = new ObservableCollection<Tuple<string,MinimapObjectView,Transform>>();
		_fastObservedObjects = new ObservableCollection<Tuple<string,MinimapObjectView,Transform>>();
		Ic.Player.ObserveEveryValueChanged(changedTransform =>changedTransform.eulerAngles)
			.Subscribe(angles => {
				PlayerMiniIcon.eulerAngles=angles;
			});
		_fastObservedObjects.ListenRemove((keyval) =>
		{
			keyval.Item2.SetActiveSafe(false);
		});
		_fastObservedObjects.ListenAdd((keyval) =>
		{
			return keyval;
		});
		Ic.Enemies.ListenAdd(enemy =>
		{
			//Создание новой точки противника, и добавление его в список отслеживаемых
			var keyval = new Tuple<string, MinimapObjectView, Transform>("enemy", TargetsPool.GetUnusedObject("enemy"), enemy.Value.CashedTransform);
			
			_fastObservedObjects.Add(keyval);
			Ic.Enemies.ListenRemove(enemy, () =>
			{
				keyval.Item2.SetActiveSafe(false);
				keyval.Item2.Reuse();
				_slowObservedObjects.Remove(keyval);
				_fastObservedObjects.Remove(keyval);
			},true);
			return enemy;
		});
		
		Ic.Asteroids.ListenAdd(asteroid =>
		{
			//Создание новой точки астероида, и добавление его в список отслеживаемых
			var keyval = new Tuple<string, MinimapObjectView, Transform>("asteroid", TargetsPool.GetUnusedObject("asteroid"), asteroid.Value.ContainerTransform);
			//получение пригодных для миникарты данных о позиции
			_fastObservedObjects.Add(keyval);
			Ic.Asteroids.ListenRemove(asteroid, () =>
			{
				keyval.Item2.SetActiveSafe(false);
				keyval.Item2.Reuse();
				_slowObservedObjects.Remove(keyval);
				_fastObservedObjects.Remove(keyval);
			},true);
			return asteroid;
		});
		
		Ic.Player.ObserveEveryValueChanged(changedTransform =>changedTransform.position)
			.Subscribe(pos => {
				//PlayerMiniIcon.eulerAngles=angles;
			});
		StartCoroutine(CheckFastObjects());
		StartCoroutine(CheckSlowObjects());
	}

	public IEnumerator CheckFastObjects()
	{
		float distance;
		while (true)
		{
			
			for (int i = 0; i < _fastObservedObjects.Count; i++)
			{
				distance = Vector2.Distance(_fastObservedObjects[i].Item3.position, Ic.Player.position);
				if (distance> MaxDistanceVision)
				{
					_slowObservedObjects.Add(_fastObservedObjects[i]);
					_fastObservedObjects.Remove(_fastObservedObjects[i]);
					i--;
					continue;
				}
				PlaceMinimapPoint(_fastObservedObjects[i].Item2, _fastObservedObjects[i].Item3.position, Ic.Player.position);
			}
			yield return new WaitForFixedUpdate();
		}
	}

	public IEnumerator CheckSlowObjects()
	{
		float distance;
		while (true)
		{
			for (int i = 0; i < _slowObservedObjects.Count; i++)
			{
				distance = Vector2.Distance(_slowObservedObjects[i].Item3.position, Ic.Player.position);
				if (distance < MaxDistanceVision)
				{
					_fastObservedObjects.Add(_slowObservedObjects[i]);
					_slowObservedObjects.Remove(_slowObservedObjects[i]);
					i--;
					continue;
				}
			}
			yield return new WaitForSeconds(0.5f);
		}
	}

	public void PlaceMinimapPoint(MinimapObjectView view,Vector2 targetPos,Vector2 playerPos)
	{
		
		//получение пригодных для миникарты данных о позиции
		var posDiff = playerPos - targetPos;
			
		view.CashedRectTransform.localPosition = new Vector2(-posDiff.x/coeficiente,-posDiff.y/coeficiente);
		view.SetActiveSafe(true);
	}
}
