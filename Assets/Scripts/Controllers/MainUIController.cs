﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainUIController : MonoBehaviour
{

	public static MainUIController Instance;
	public Text EnemiesCounter;
	public int KillCount=0;

	public GameObject AbilityPrefab;
	public Transform AbilsHolder;
	
	public Button Hud;
	public GameObject UpgradesObject;

	public Button UpgradeButton;
	public GameObject UpgradeArrow;

	public RectTransform JoystickHolder;
	public JoystickController JoystickControl;
	public JoystickController JoystickAttack;

	public CanvasGroup FightUI;
	public CanvasGroup TopMostUI;
	public Button PlayerUpgradeTarget;

	public List<UIWindow> AllWindows;

	public List<KeyValuePair<GameObject, IDisposable>> AbilsActivates;

	public SmallWeaponInfo WeaponInfo;

	public Text TempDebug;
	
	public GraphicRaycaster UIRaycaster;
	public EventSystem GameEventSystem;

	private void Awake()
	{
		Instance = this;
	}

	private void Update()
	{
		var vel = InstanceController.Instance.Player.GetComponent<Rigidbody2D>().velocity;      //to get a Vector3 representation of the velocity
		TempDebug.text = vel.magnitude.ToString();  
	}

	public void Init()
	{
		AbilsActivates = new List<KeyValuePair<GameObject, IDisposable>>();
		InstanceController.Instance.ScrapCollected.Subscribe(val => EnemiesCounter.text = val.ToString());
		UpgradeButton.onClick.AddListener(()=>
		{
			UpgradesChangeState(true);
		});
		
		Hud.onClick.AddListener(()=>
		{
			UpgradesChangeState(false);
		});
		WeaponInfo.InitializeShipInfo(InstanceController.Instance.PlayerData);
	}

	public void InitAbilities(Ship playerShip)
	{
		for (int i = 0; i < AbilsHolder.childCount; i++)
		{
			Destroy(AbilsHolder.GetChild(i).gameObject);
		}
		AbilsHolder.DetachChildren();
		var abils = playerShip.Abilities;
		for (int i = 0; i < abils.Count; i++)
		{
			var abilImg = Instantiate(AbilityPrefab, AbilsHolder).GetComponent<Image>();
			var upgradeTarg = abilImg.GetComponent<LevelUpTarget>();
			upgradeTarg.UpgradeParts.Add(abils[i]);
			UpgradeController.RegisterUpgradeTargets(upgradeTarg, upgradeTarg.transform);
			BindAbilityActivator(abilImg,abils[i],playerShip);
		}
	}

	public void BindAbilityActivator(Image target, Ability ability, Ship playerShip)
	{
		var activatePair = AbilsActivates.FirstOrDefault(pair => pair.Key == target.gameObject);
		if (!activatePair.Equals(default(KeyValuePair<GameObject,IDisposable>)))
		{
			AbilsActivates.Remove(activatePair);
			activatePair.Value.Dispose();
		}
		var reloadImg = target.transform.GetChild(0).GetComponent<Image>();
		IDisposable abilActivate = null;
		abilActivate = target.OnPointerClickAsObservable().Subscribe(pointer =>
		{
			if (_upgradeMod) return;
			var reactiveProperty = playerShip.ActivateAbility(ability.Id);
			if (reactiveProperty==null) return;
			reloadImg.SetActiveSafe(true);
			IDisposable disp = null;
			disp = reactiveProperty.Subscribe(nextVal =>
			{
				reloadImg.fillAmount = nextVal/ability.ReloadTime;
				if (nextVal < Double.Epsilon)
				{
					reloadImg.SetActiveSafe(false);
					disp.Dispose();
				}
			});
		});
		target.sprite = Resources.Load<Sprite>("Sprites/Abilities/"+ability.IconName);
		AbilsActivates.Add(new KeyValuePair<GameObject, IDisposable>(target.gameObject, abilActivate));
	}
    public void BindAbilityUpgrade(Image target, LevelUpTarget lvlUpTarget)
	{
		_abilsUpgradesConnector.add = target.OnPointerClickAsObservable().Subscribe(pointer =>
		{
			ShowUIWindow(WindowTypeEnum.UpgradeWindow, lvlUpTarget);
		});
	}
	public void UpgradeButtonChangeState(bool state)
	{
		UpgradeArrow.SetActiveSafe(state);
	}

	public void UpgradesChangeState(bool state)
	{
		FightUI.blocksRaycasts = !state;
		TopMostUI.blocksRaycasts = true;
		Hud.SetActiveSafe(state);
		JoystickHolder.SetActiveSafe(!state);
		Time.timeScale = state ? 0 : 1;
		if (state)
			SetUpgradesView();
		else
			ResetUpgradesView();
	}

	private Transform _lastAbilsParent;
	private ConnectionsCollector _abilsUpgradesConnector = new ConnectionsCollector();
	private bool _upgradeMod = false;
	
	public void SetUpgradesView()
	{
		//InitUpgradesView
		_abilsUpgradesConnector.DisconnectAll();
		_upgradeMod = true;
		PlayerUpgradeTarget.SetActiveSafe(true);
		PlayerUpgradeTarget.transform.eulerAngles = InstanceController.Instance.Player.eulerAngles;
		UpgradesObject.SetActiveSafe(true);
		_lastAbilsParent = AbilsHolder.parent;
		AbilsHolder.SetParent(UpgradesObject.transform);
		var playerPosition = InstanceController.Instance.Player.position;
		PlayerUpgradeTarget.transform.position = playerPosition;
		InstanceController.Instance.Player.position = new Vector3(playerPosition.x,playerPosition.y,-7);
		var playerShip = InstanceController.Instance.PlayerData;
		//BindAbilUpgrades
		for (int i = 0; i < AbilsHolder.childCount; i++)
		{
			var abilView = AbilsHolder.GetChild(i).GetComponent<Image>();
			var lvlUpTarget = abilView.GetComponent<LevelUpTarget>();
			BindAbilityUpgrade(abilView, lvlUpTarget);
		}

		_abilsUpgradesConnector.add = Disposable.Create(PlayerUpgradeTarget.onClick.RemoveAllListeners);
		var targ = PlayerUpgradeTarget.GetComponent<LevelUpTarget>();
		PlayerUpgradeTarget.onClick.AddListener(() =>
		{
			targ.UpgradeParts.Clear();
			targ.UpgradeParts.AddRange(playerShip.ShipComponents.Select(component=>component as IUpgradable));
			targ.UpgradeParts.AddRange(playerShip.Weapons.Select(weapon=>weapon as IUpgradable));
			ShowUIWindow(WindowTypeEnum.UpgradeWindow, targ);
		});
	}

	public void ResetUpgradesView()
	{
		_upgradeMod = false;
		_abilsUpgradesConnector.DisconnectAll();
		PlayerUpgradeTarget.SetActiveSafe(false);
		UpgradesObject.SetActiveSafe(false);
		AbilsHolder.SetParent(_lastAbilsParent);
	}

	public void ShowUIWindow(WindowTypeEnum windowType, object data)
	{
		var result = AllWindows.FirstOrDefault(window => window.WindowType == windowType);
		if (result!=null)
			result.Show(data);
		else
			Debug.LogError("No window with type '"+windowType.ToString()+"' in list");
	}
}
