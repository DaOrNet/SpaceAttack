﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UniRx;
using UnityEngine;

public class DataController
{
	public UniRx.ReactiveProperty<Vector2> MovementJoystick = new ReactiveProperty<Vector2>();
	public UniRx.ReactiveProperty<Vector2> AttackJoystick = new ReactiveProperty<Vector2>();
}

