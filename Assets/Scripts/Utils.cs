﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;    
using Newtonsoft.Json;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public static class Utils
{
    private static JsonSerializerSettings _jsonSettings;

    static Utils()
    {
        _jsonSettings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
    }
    public static void SetActiveSafe(this GameObject obj, bool enable)
    {
        if (obj == null) return;
        if (obj.activeSelf==enable) return;
        obj.SetActive(enable);
    }
    public static void SetActiveSafe(this Component obj, bool enable)
    {
        if (obj == null) return;
        if (obj.gameObject.activeSelf==enable) return;
        obj.gameObject.SetActive(enable);
    }
    
    public static IEnumerable<T> ForEach<T>(this IEnumerable<T> value, Action<T> act)
    {
        foreach (var v in value)
        {
            act(v);
        }

        return value;
    }

    public static T CloneByJson<T>(this T obj)
    { 
        var serialized = JsonConvert.SerializeObject(obj,_jsonSettings);
        return JsonConvert.DeserializeObject<T>(serialized,_jsonSettings);
    }

    public static void DrawPolygon(Polygon polygon, Transform parent)
    {
#if UNITY_EDITOR
        Handles.color = Color.red;
        for (int i = 1; i < polygon.Vertices.Count; i++)
        {
            Handles.DrawLine(parent.position+polygon.Vertices[i-1],parent.position+polygon.Vertices[i]);
        }
        Handles.DrawLine(parent.position+polygon.Vertices[polygon.Vertices.Count-1],parent.position+polygon.Vertices[0]);
#endif
    }

    public static float ParceToFloat(this string obj)
    {
        return float.Parse(obj);
    }
    public static Vector2 DegreeToVector2(this float degree)
    {
        return (Quaternion.Euler(0, 0, degree) * Vector2.right);
    }

    public static void AddForceSave(this Rigidbody2D rigidbody2D, Vector3 force, float speedLimit)
    {
        if (rigidbody2D.velocity.magnitude < speedLimit)
            rigidbody2D.AddForce(force);
    }
}

public interface IPoolable
{
    Action OnFree { get; set; }
    IEnumerator WaitForReuse(float delay);
    void Reuse();
}
//TODO: Сделать IDisposable чтоб не получить переполнение подписок
public class ObservableCollection<T>
{
    public int Count
    {
        get { return _collection.Count; }
    }

    public T this[int key]
    {
        get
        {
            return _collection[key];
        }
        set
        {
            _collection[key] = value;
        }
    }

    public ObservableCollection()
    {
        _removeActionsForTarg = new Dictionary<T, List<Tuple<Action,bool>>>();
        _addActions = new List<Tuple<Func<T,T>,bool>>();
        _removeActionsForAll = new List<Tuple<Action<T>, bool>>();
        _collection = new List<T>();
    }

    public void Add(T value)
    {
        _collection.Add(value);
        _addActions.ForEach(act => value = act.Item1.Invoke(value));
        _addActions.RemoveAll(act => act.Item2);
    }

    private List<T> _collection;

    public void RemoveAll(T value)
    {
        while (Remove(value))
        {
            
        }
    }
    public void RemoveAll(Func<T,bool> condition)
    {
        for (int i = 0; i < _collection.Count; i++)
        {
            if (condition.Invoke(_collection[i]))
            {
                Remove(_collection[i]);
                i--;
            }
        }
    }
    public bool Remove(T value)
    {
        bool result;
        if (_removeActionsForTarg.ContainsKey(value))
        {
            result = _collection.Remove(value);
            if (result)
            {
                var toDeleteList = _removeActionsForTarg[value].Where(act => act.Item2).ToArray();
                _removeActionsForTarg[value].RemoveAll(act => act.Item2);
                toDeleteList.ForEach(act => act.Item1.Invoke());
            }
        }
        else
        {
            result = _collection.Remove(value);
        }

        _removeActionsForAll.ForEach(keyval => keyval.Item1.Invoke(value));
        _removeActionsForAll.RemoveAll(keyval => keyval.Item2);
        return result;
    }

    public bool Contains(T value)
    {
        return _collection.Contains(value);
    }

    private List<Tuple<Func<T,T>,bool>> _addActions;

    public IDisposable ListenAdd(Func<T,T> onAdd,bool oneTime=false)
    {
        var tuple = new Tuple<Func<T,T>, bool>(onAdd, oneTime);
        _addActions.Add(tuple);
        return Disposable.Create(() =>
        {
            if (_addActions.Contains(tuple))
            {
                _addActions.Remove(tuple);
            }
        });
    }
    
    
    private Dictionary<T, List<Tuple<Action,bool>>> _removeActionsForTarg;
    private List<Tuple<Action<T>,bool>> _removeActionsForAll;

    public IDisposable ListenRemove(T value, Action onRemove,bool oneTime=false)
    {
        var removeTuple = new Tuple<Action, bool>(onRemove, oneTime);
        if (_removeActionsForTarg.ContainsKey(value))
        {
            _removeActionsForTarg[value].Add(removeTuple);
        }
        else
        {
            _removeActionsForTarg.Add(value, new List<Tuple<Action, bool>>());
            _removeActionsForTarg[value].Add(removeTuple);
        }

        return Disposable.Create(() =>
        {
            if (_removeActionsForTarg.ContainsKey(value))
            {
                _removeActionsForTarg[value].Remove(removeTuple);
            }
        });
    }
    
    public IDisposable ListenRemove(Action<T> onRemove,bool oneTime=false)
    {
        var removeTuple = new Tuple<Action<T>, bool>(onRemove, oneTime);
        _removeActionsForAll.Add(removeTuple);

        return Disposable.Create(() =>
        {
            _removeActionsForAll.Remove(removeTuple);
        });
    }
}
[Serializable]
public class ObjectPool<T> where T:class,IPoolable
{
    protected List<T> _pool;
    protected List<T> _activeObjects;
    protected Func<T> _createAction;
    public ObjectPool(Func<T> createAction)
    {
        _createAction = createAction;
        _pool = new List<T>();
        _activeObjects = new List<T>();

    }

    public T GetUnusedObject()
    {
        T currentObj;
        if (_pool.Count > 0)
        {
            currentObj = _pool[0];
            _pool.RemoveAt(0);
            _activeObjects.Add(currentObj);
            return currentObj;
        }
        
        currentObj = _createAction.Invoke();
        _activeObjects.Add(currentObj);
        currentObj.OnFree = () =>
        {
            _activeObjects.Remove(currentObj);
            _pool.Add(currentObj);
        };
        return currentObj;
    }
}

public class ManyObjectsPool<T> where T:class,IPoolable
{
    protected List<KeyValuePair<string,List<T>>> _pools;
    protected List<KeyValuePair<string,List<T>>> _activeObjects;
    protected List<KeyValuePair<string,Func<T>>> _createActions;
    protected List<KeyValuePair<string,bool>> _special;
    public ManyObjectsPool(List<KeyValuePair<string,Func<T>>> createActions,List<KeyValuePair<string,bool>> specials)
    {
        _pools = new List<KeyValuePair<string, List<T>>>();
        _activeObjects = new List<KeyValuePair<string, List<T>>>();
        _createActions = createActions;
        _special = specials;
        for (int i = 0; i < createActions.Count; i++)
        {
            _pools.Add(new KeyValuePair<string,List<T>>(createActions[i].Key,new List<T>()));
            _activeObjects.Add(new KeyValuePair<string,List<T>>(createActions[i].Key,new List<T>()));
        }
    }
    
    public T GetUnusedObject(string key)
    {
        if (_createActions.All(val => val.Key != key)) return null;
        var pool = _pools.First(val => val.Key == key).Value;
        var activeObjects = _activeObjects.First(val => val.Key == key).Value;
        var func = _createActions.First(val => val.Key == key).Value;
        T currentObj;
        if (ObjectsLimit != -1 && activeObjects.Count >= _objectsLimit)
        {
            activeObjects[0].Reuse();
        }
        if (pool.Count > 0)
        {
            currentObj = pool[0];
            pool.RemoveAt(0);
            activeObjects.Add(currentObj);
            return currentObj;
        }
        
        currentObj = func.Invoke();
        activeObjects.Add(currentObj);
        currentObj.OnFree = () =>
        {
            pool.Add(currentObj);
            activeObjects.Remove(currentObj);
        };
        return currentObj;
    }

    public List<T> GetObjectsInUse(string key)
    {
        var active = _activeObjects.FirstOrDefault(pair => pair.Key == key);
        return active.Equals(default(KeyValuePair<string,T>))?null:active.Value;
    }
    public List<T> GetAllObjectsInUse()
    {
        return _activeObjects.Where(pair=>!_special.First(spec=>spec.Key==pair.Key).Value).SelectMany(pair => pair.Value).ToList();
    }

    private int _objectsLimit = -1;

    public int ObjectsLimit
    {
        get { return _objectsLimit; }
        set { _objectsLimit = value; }
    }
}

[Serializable]
public class ParentHolder<T>
{
    [NonSerialized] public T root;
    [NonSerialized] public InstanceController ic = InstanceController.Instance;
    [NonSerialized] public GameController gc = GameController.Instance;
}

public class ParentHolderMono<T> : MonoBehaviour
{
    [NonSerialized] public T root;
    [NonSerialized] public InstanceController ic;
}

[Serializable]
public class Polygon
{
    public List<Vector3> Vertices { get; private set; }
    public Polygon(List<Vector3> vertices)
    {
        Vertices = vertices;
    }
    public bool ContainsVector(Vector3 vector)
    {
        // do the magic
        return true;
    }
}

public class ConnectionsCollector
{
    private List<IDisposable> _disposables = new List<IDisposable>();

    public IDisposable add
    {
        set
        {
            if (value!=null)
                _disposables.Add(value);
        }
    }

    public void DisconnectAll()
    {
        foreach (var disposable in _disposables)
        {
            disposable.Dispose();
        }
        _disposables.Clear();
    }
}