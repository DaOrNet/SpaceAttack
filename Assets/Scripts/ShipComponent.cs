﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ShipComponent : ParentHolder<Ship>,IUpgradable
{
    public string Id;
    public string NextLevelId;
    public string NextLevelKey {
        get { return NextLevelId; }
    }
    public virtual IEnumerator InternalWork()
    {
        yield break;
    }
    
}
[Serializable]
public class ScrapCollector : ShipComponent
{
    public float StartRange = 1f;
    public float MoveSpeed = 1f;
    private float _refreshTime=1f;
    public override IEnumerator InternalWork()
    {
        var timeElapsed = 0f;
        var scrapsData = ObjectsInRange();
        while (true)
        {
            if (timeElapsed >= _refreshTime)
            {
                scrapsData = ObjectsInRange();
                timeElapsed = 0;
            }

            timeElapsed += Time.deltaTime;
            MoveObjectsToShip(scrapsData);
            yield return new WaitForFixedUpdate();
        }
    }

    public List<ScrapScript> ObjectsInRange()
    {
        return ic.ScrapsPools.GetAllObjectsInUse();
    }

    public void MoveObjectsToShip(List<ScrapScript> objectsForPull)
    {
        foreach (var scrapScript in objectsForPull)
        {
            Vector2 scrapPos = scrapScript.CashedTransform.position;
            Vector2 playerPos = root.root.CashedTransform.position;
            if (Vector2.Distance(scrapPos, playerPos) < StartRange)
            {
                scrapPos += (playerPos - scrapPos).normalized * MoveSpeed*Time.deltaTime;
                scrapScript.CashedRigidbody2D.MovePosition(scrapPos);
            }
        }
    }
}
