﻿using System.Collections;
using System;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using UniRx;
using UnityEngine;

[Serializable]
public class Ability : ParentHolder<Ship>,IUpgradable
{
    public string Id;
    public string IconName;
    public float ReloadTime;
    
    public string NextLevelId;

    public string NextLevelKey {
        get { return NextLevelId; }
    }
    
    public virtual IEnumerator Do(Ship target)
    {
        yield break;
    }
    public virtual IEnumerator Show()
    {
        yield break;
    }
}

//Кастует непробиваемый щит на корабль, восстанавливает поврежденные щиты
[Serializable]
public class ImmortalShield : Ability
{
    
    
    public override IEnumerator Do(Ship target)
    {
        yield break;
    }
    public override IEnumerator Show()
    {
        yield break;
    }
}

//Корабль выпускает дронов - ремонтников
[Serializable]
public class RepairDrones : Ability
{
    private Drone[] _drones;
    
    public override IEnumerator Do(Ship target)
    {
        yield break;
    }
    public override IEnumerator Show()
    {
        yield break;
    }
}
[Serializable]
public class Drone
{
    
}
[Serializable]
public class RepairDrone : Drone
{
    private float _speed;
    private float _repairValue;
    
    public IEnumerator DoRepair()
    {
        yield break;
    }
}

[Serializable]
public class ShieldRestore : Ability
{
    public float shieldChargeBonuse;
    public float shieldChargeTime;
    public override IEnumerator Do(Ship target)
    {
        target.Shields.Value = target.MaxShields;
        bool ticking = true;

        GameController.Instance.SetTimer(() =>
        {
            if (target != null && target.Alive.Value)
            {
                ticking = false;
            }
        }, new ReactiveProperty<float>(shieldChargeTime));
        while (ticking)
        {
            if (target.Shields.Value<target.MaxShields)
                target.Shields.Value += shieldChargeBonuse;
            yield return new WaitForSeconds(0.1f);
        }
        yield break;
    }
    public override IEnumerator Show()
    {
        yield break;
    }
}

[Serializable]
public class Autoaim : Ability
{
    public override IEnumerator Do(Ship target)
    {
        yield break;
    }
    public override IEnumerator Show()
    {
        yield break;
    }
}

//Оставляет под собой мину
[Serializable]
public class PlaceMine : Ability
{
    private Mine _mineExample;
    public override IEnumerator Do(Ship target)
    {
        yield break;
    }
    public override IEnumerator Show()
    {
        yield break;
    }
}
[Serializable]
public class Mine
{
    
}
[Serializable]
public class DamageMine : Mine
{
    private float _damage;
    private DamageTypeEnum _damageType;
    private float _damageRadius;
    
}

