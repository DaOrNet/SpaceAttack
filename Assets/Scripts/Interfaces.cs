﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interfaces {
}

public interface IDamagable
{
    void GetDamage(DamageTypeEnum damageType,float damage,MonoBehaviour sender);
    void GetCriticalDamage(MonoBehaviour sender);
    float CheckDamagedStatus(MonoBehaviour sender);
}

public interface IScrapCollector
{
    bool Collectable { get; }
    void CollectScrap(Scrap scrap);
}

public interface IUpgradable
{
    string NextLevelKey { get; }
}
public interface IUpgradeTarget
{
    List<IUpgradable> UpgradeParts { get; }
}