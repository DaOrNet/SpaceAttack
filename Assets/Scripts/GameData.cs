﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class GameData
{
	public static GameData Instance;

	public float MinScrapValue;
	public int MaxScrapPacks;
	
	public Ship[] AllShips;
	public Asteroid[] Asteroids;
	public List<KeyValuePair<string,Ability>> Abilities;
	public List<KeyValuePair<string, Scrap>> ScrapsData;
	public List<KeyValuePair<string, Weapon>> WeaponsData;
	public List<KeyValuePair<string, ShipComponent>> ShipComponentsData;
	public List<KeyValuePair<string, float>> UpgradesData;
	
	public static GameData LoadGameData()
	{
		GameData game = null;
		byte[] bin = null;
		AssetBundle bundle = AssetBundle.LoadFromFile(Application.streamingAssetsPath+"/gd.asset") ;
		var textAsset = bundle.LoadAsset<TextAsset>("gd");
		bin = textAsset.bytes;
		if (bin != null)
		{
			var stream = new MemoryStream(bin);
			BinaryFormatter formatter = new BinaryFormatter();
			formatter.AssemblyFormat=FormatterAssemblyStyle.Simple;
			game = formatter.Deserialize(stream) as GameData;
		}
		else
		{
			throw new Exception("game data load error");
		}
		bundle.Unload(true);
		return game;
	}

}
