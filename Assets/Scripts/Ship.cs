﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UniRx;
using UnityEngine;

[Serializable]
public class Ship : ParentHolder<ShipController>,IDamagable
{
	public string Id;
	public List<Weapon> Weapons = new List<Weapon>();
	[JsonIgnore][NonSerialized]
	public ReactiveProperty<int> SelectedWeapon;
	public List<Ability> Abilities = new List<Ability>();
	public float MaxShields;
	[JsonIgnore][NonSerialized]
	public ReactiveProperty<float> Shields;
	public float MaxHp;
	[JsonIgnore][NonSerialized]
	public ReactiveProperty<float> Hp;
	public float ShieldsRegenValue;
	public float HpRegenValue;
	public float EnginePower;

	public string ShipPrefabPath;
	public string ShipImagePath;

	[JsonIgnore][NonSerialized]
	public float WeaponRotation;
	
	public float ShipScale;
	
	public float Worth;
	public List<ShipComponent> ShipComponents;

	[SerializeField]
	private float _mobility = 10;

	public float Mobility
	{
		get { return _mobility; }
		set { _mobility=value; }
	}

	[NonSerialized] public ReactiveProperty<bool> FireOn;
	[NonSerialized] public ReactiveProperty<bool> Alive;
	
	public void Init()
	{
		Alive = new ReactiveProperty<bool>(true);
		Shields = new ReactiveProperty<float>(MaxShields);
		Hp = new ReactiveProperty<float>(MaxHp);
		SelectedWeapon = new ReactiveProperty<int>(0);
		ShipComponents.ForEach(part => part.root = this);
		Weapons.Where(weapon=>weapon!=null).ForEach(weapon => { 
			weapon.root = this;
			weapon.Init();
		});
		Abilities.Where(ability=>ability!=null).ForEach(ability => ability.root = this);
		FireOn=new ReactiveProperty<bool>();
	}
	[NonSerialized]
	public ObservableCollection<object> CurrentActions = new ObservableCollection<object>();
	
	public void GetDamage(DamageTypeEnum damageType,float damage, MonoBehaviour sender)
	{
		Shields.Value -= damage;
		if (Shields.Value < 0)
		{
			_shieldsRegenTime = 5f;
			damage = Mathf.Abs(Shields.Value);
			Shields.Value = 0;
			_hpRegenTime = 5f;
		}
		else
		{
			damage = 0;
			_shieldsRegenTime = 5f;
		}
		Hp.Value -= damage;
		if (Hp.Value <= 0)
		{
			GetCriticalDamage(sender);
		}
	}

	public void GetCriticalDamage(MonoBehaviour sender)
	{
		Alive.Value = false;
		root.GetCriticalDamage(sender);
	}

	public float CheckDamagedStatus(MonoBehaviour sender)
	{
		throw new System.NotImplementedException();
	}

	public ReactiveProperty<float> ActivateAbility(string id)
	{
		if (CurrentActions.Contains(id) || !AbilityActivator(id))
			return null;
		CurrentActions.Add(id);
		var property = new ReactiveProperty<float>(Abilities.First(abil => abil.Id == id).ReloadTime);
		GameController.Instance.SetTimer(() => { CurrentActions.Remove(id); },property);
		return property;
	}
	
	public bool AbilityActivator(string id)
	{
		var abil = Abilities.FirstOrDefault(ability => ability.Id == id);
		if (abil == null)
			return false;
		root.StartCoroutine(abil.Do(this));
		root.StartCoroutine(abil.Show());
		return true;
	}

	private float _shieldsRegenTime=1f;
	private float _hpRegenTime=1f;
	public IEnumerator InternalWork()
	{
		while (true)
		{
			if (Shields.Value < MaxShields)
			{
				_shieldsRegenTime -= Time.deltaTime;
				if (_shieldsRegenTime < 0)
				{
					_shieldsRegenTime = 1f;
					Shields.Value = Shields.Value + ShieldsRegenValue < MaxShields ? Shields.Value + ShieldsRegenValue : MaxShields;
				}
			}

			if (Hp.Value < MaxHp)
			{
				_hpRegenTime -= Time.deltaTime;
				if (_hpRegenTime < 0)
				{
					_hpRegenTime = 1f;
					Hp.Value = Hp.Value + HpRegenValue < MaxHp ? Hp.Value + HpRegenValue : MaxHp;
				}
			}
			yield return new WaitForFixedUpdate();
		}
	}

	public void Fire()
	{
		if (FireOn.Value && !CurrentActions.Contains(Weapons[SelectedWeapon.Value].GetType()))
		{
			root.StartCoroutine(Weapons[SelectedWeapon.Value].Do());
			root.StartCoroutine(Weapons[SelectedWeapon.Value].Show());
			root.Collector.add = CurrentActions.ListenRemove(Weapons[SelectedWeapon.Value].GetType(), Fire, true);
		}
	}
}
