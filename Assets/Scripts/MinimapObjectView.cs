﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class MinimapObjectView : ParentHolderMono<MinimapController>,IPoolable
{

	public Transform CashedTransform;
	public RectTransform CashedRectTransform;
	
	public void Init()
	{
		CashedTransform = transform;
		CashedRectTransform = GetComponent<RectTransform>();
	}

	public Action OnFree { get; set; }
	public IEnumerator WaitForReuse(float delay)
	{
		throw new NotImplementedException();
	}

	public void Reuse()
	{
		OnFree.Invoke();
	}
}
