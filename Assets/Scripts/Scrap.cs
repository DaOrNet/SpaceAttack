﻿using System;
[Serializable]
public class Scrap : ParentHolder<ScrapScript>
{

	public string Id;
	public float MinScrapCount;
	public float MaxScrapCount;
	private float _scrapCount;

	public float ScrapCount
	{
		get { return _scrapCount; }
	}

	public string PrefabPath;
	public string ImagePath;
	public float SizeX;
	public float SizeY;

	public void Init(float scrapCount,ScrapScript parent)
	{
		root = parent;
		_scrapCount = scrapCount;
	}
}
