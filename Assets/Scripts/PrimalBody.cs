﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrimalBody
{

	public Ship[] AvalibleShips;
	public Ship[] OpenedShips;

	public byte OpenedLevel;

	public byte SelectedShip = 0;
	
	private static PrimalBody _instance;
	
	public static PrimalBody Instance
	{
		get { return _instance ?? (_instance = new PrimalBody()); }
	}

	public static PrimalBody StringToPrimalBody(string body)
	{
		//TODO: Сделать при помощи JSON
		return new PrimalBody();
	}

	public static string PrimalBodyToString(PrimalBody body)
	{
		//TODO: Сделать при помощи JSON
		return "";
	}

	public void UnlockShip(int id)
	{
		
	}
	
	public void WakeUp()
	{
		
	}	

	private PrimalBody()
	{
		AvalibleShips = GameData.Instance.AllShips.CloneByJson();
		OpenedShips = new Ship[]{(Ship)AvalibleShips[0].CloneByJson()};
	}
}
