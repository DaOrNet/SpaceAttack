﻿using System.Collections;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UniRx;
using UnityEngine;
[Serializable]
public class Weapon : ParentHolder<Ship>,IUpgradable
{
	public string Id;
	public string WeaponPrefabToLoad;
	public string NextLevelId;
	public string NextLevelKey {
		get { return NextLevelId; }
	}
	[JsonIgnore][NonSerialized]
	public ConnectionsCollector Collector;
	public virtual void Init()
	{
		Collector = new ConnectionsCollector();
	}
	public virtual IEnumerator Do()
	{
		yield break;
	}

	public virtual IEnumerator Show()
	{
		yield break;
	}

	public virtual void ToSleep()
	{
		Collector.DisconnectAll();
	}

	public virtual ReactiveProperty<float> GetInfoCount()
	{
		return null;
	}

	public virtual IDisposable SubscribeToStatus(Action<string> statusAction)
	{
		statusAction.Invoke("-1");
		return Disposable.Empty;
	}
}

[Serializable]
public class LaserRay : Weapon
{
	public float Damage;
	public DamageTypeEnum DamageType;

	public float MaxDistance;

	public ReactiveProperty<float> Energy;
	public float MaxEnergy;
	public float EnergyPerUnit;
	public float EnergyRestorePerSecond;
	[JsonIgnore][NonSerialized]
	public Color BeamColor;

	public float[] ColorArray;
	[JsonIgnore][NonSerialized]
	private LineRenderer _beamRenderer;
	private int _targetsLayer;

	public override void Init()
	{
		base.Init();
		RestoreCollector = new ConnectionsCollector();
		BeamColor = new Color(ColorArray[0],ColorArray[1],ColorArray[2]);
		Collector.add = Disposable.Create(() => { GameObject.Destroy(_beamRenderer.gameObject);});
		_beamRenderer = GameObject.Instantiate(ic.GetFromCashe(WeaponPrefabToLoad), root.root.WeaponsHolder)
			.GetComponent<LineRenderer>();
		_beamRenderer.startColor = BeamColor;
		_beamRenderer.endColor = BeamColor;
		Energy = new ReactiveProperty<float>(MaxEnergy);
		_targetsLayer = 1 << LayerMask.NameToLayer("PhysicalObjects");
	}
	[JsonIgnore][NonSerialized]
	private KeyValuePair<Collider2D,IDamagable> _lastRaycastDamagableObject;
	public override IEnumerator Do()
	{
		if (Energy.Value<3)
			yield break;
		root.CurrentActions.Add(GetType());
		RestoreCollector.DisconnectAll();
		ic.StopCoroutine(RestoreEnergy());
		while (root.FireOn.Value && Energy.Value > EnergyPerUnit)
		{
			var hit = Physics2D.RaycastAll(root.root.CashedTransform.position, root.WeaponRotation.DegreeToVector2(), MaxDistance,
				_targetsLayer);
			if (hit.Length>1)
			{
				if(_lastRaycastDamagableObject.Value==null || _lastRaycastDamagableObject.Key != hit[1].collider)
					_lastRaycastDamagableObject = new KeyValuePair<Collider2D, IDamagable>(hit[1].collider,
						hit[1].collider.attachedRigidbody.GetComponent<IDamagable>());
				if (_lastRaycastDamagableObject.Value!=null)
					_lastRaycastDamagableObject.Value.GetDamage(DamageType,Damage,root.root);
			}

			Energy.Value -= EnergyPerUnit;
			yield return new WaitForFixedUpdate();
		}
		root.CurrentActions.Remove(GetType());
		ic.StartCoroutine(RestoreEnergy());
	}

	public override IEnumerator Show()
	{
		if (Energy.Value<3)
			yield break;
		_beamRenderer.SetActiveSafe(true);
		while (root.FireOn.Value && Energy.Value > EnergyPerUnit)
		{
			_beamRenderer.SetPosition(0,root.root.CashedTransform.position);
			var hit = Physics2D.RaycastAll(root.root.CashedTransform.position, root.WeaponRotation.DegreeToVector2(), MaxDistance,
				_targetsLayer);
			Vector2 endpoint;
			if (hit.Length>1)
			{
				endpoint = hit[1].point;
				_beamRenderer.SetPosition(1,endpoint);
			}
			else
			{
				endpoint = root.WeaponRotation.DegreeToVector2() * MaxDistance;
				_beamRenderer.SetPosition(1,(Vector2)root.root.CashedTransform.position+endpoint);
			}
			yield return null;
		}
		_beamRenderer.SetActiveSafe(false);
	}
	[JsonIgnore][NonSerialized]
	public ConnectionsCollector RestoreCollector;
	public IEnumerator RestoreEnergy()
	{
		yield return new WaitForSeconds(2f);
		var timer = new ReactiveProperty<float>((MaxEnergy-Energy.Value)/EnergyRestorePerSecond);
		var lastValue = timer.Value;
		Collector.add = RestoreCollector.add = timer.Subscribe(val =>
		{
			var restored = (lastValue - val) * EnergyRestorePerSecond;
			Energy.Value += restored;
			lastValue = val;
		});
		Collector.add = RestoreCollector.add = GameController.Instance.SetTimer(() => { RestoreCollector.DisconnectAll(); }, timer);
	}

	public override IDisposable SubscribeToStatus(Action<string> statusAction)
	{
		return Collector.add = Energy.Subscribe(next => { statusAction.Invoke(next + "/" + MaxEnergy); });
	}
}
[Serializable]
public class LaserGun : Weapon
{
	public float Damage;
	public DamageTypeEnum DamageType;

	public float MaxDistance;

	public float BulletSpeed;
	public float MaxShotsCount;
	public float ShotRestoreTime;
	public ReactiveProperty<float> LeftShots;
	public float CurrentShots;
	public string ShotPrefabPath;
	public float FireRate;
	
	public ObjectPool<Shot> _pool;

	public LaserShot BulletExample;


	private bool _isRestoring = false;
	public override void Init()
	{
		base.Init();
		_pool = new ObjectPool<Shot>(()=>MonoBehaviour.Instantiate(Resources.Load<Shot>(ShotPrefabPath)));
		CurrentShots = MaxShotsCount;
		LeftShots = new ReactiveProperty<float>(CurrentShots);
		BulletExample = new LaserShot()
		{
			Damage = Damage,
			DamageType = DamageType,
			Range = MaxDistance,
			Speed = BulletSpeed
		};
		Collector.add = LeftShots.Subscribe(val => //TODO: ЗАКОЛЛЕКТИТЬ
		{
			if (!_isRestoring)
				ic.StartCoroutine(RestoreShots());
		});
	}

	public IEnumerator RestoreShots()
	{
		_isRestoring = true;
		while (LeftShots.Value<MaxShotsCount)
		{
			yield return new WaitForSeconds(ShotRestoreTime);
			if(LeftShots.Value<MaxShotsCount)
				LeftShots.Value++;
		}
		_isRestoring = false;

	}
	
	public override IEnumerator Do()
	{
		root.CurrentActions.Add(GetType());
		if (LeftShots.Value > 0)
		{
			yield return null;
			LeftShots.Value--;
			yield return new WaitForSeconds(FireRate);
			
		}
		else
		{
			yield return new WaitForSeconds(0.2f);
		}
		root.CurrentActions.Remove(GetType());
	}

	public override IEnumerator Show()
	{
		if (LeftShots.Value > 0)
		{
			float neededRot = root.WeaponRotation;
			var res = _pool.GetUnusedObject();
			res.transform.rotation = Quaternion.AngleAxis(neededRot - 90, Vector3.forward);
			res.transform.position = root.root.transform.position;
			res.Data = BulletExample.CloneByJson();
			res.NotAcceptable = root.root.gameObject;
			res.gameObject.SetActiveSafe(true);
		}

		yield return null;
	}

	public override IDisposable SubscribeToStatus(Action<string> statusAction)
	{
		return Collector.add = LeftShots.Subscribe(next => { statusAction.Invoke(next + "/" + MaxShotsCount); });
	}
}
[Serializable]
public class RocketLauncher : Weapon
{
	private Rocket _ammunition;
	
	public new IEnumerator Do()
	{
		yield break;
	}

	public new IEnumerator Show()
	{
		yield break;
	}
}
[Serializable]
public class Rocket : DamageBullet
{
	
}
[Serializable]
public class LaserShot : DamageBullet
{
	
}
[Serializable]
public class DamageBullet
{
	public float Damage;
	public float Speed;
	public DamageTypeEnum DamageType;
	public float Range;
}