﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTimer : MonoBehaviour
{

	public float TimeToLife = 1;
	
	void Awake()
	{
		Destroy(gameObject,TimeToLife);
	}
}
