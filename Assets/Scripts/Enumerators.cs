﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enumerators{

}

public enum DamageTypeEnum
{
    Explosion,
    Laser,
    LaserRay,
    AsteroidCollision
}

public enum JoystickTypeEnum
{
    Control,
    Attack
}
