﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrapScript : MonoBehaviour,IPoolable
{

	public Scrap Data;
	public Transform CashedTransform;
	public Rigidbody2D CashedRigidbody2D;
	public SpriteRenderer CashedSpriteRenderer;

	public void Awake()
	{
		
	}
	public void Init(float scrapCount)
	{
		if (CashedTransform == null)
		{
			CashedTransform = transform;
			CashedRigidbody2D = GetComponent<Rigidbody2D>();
			CashedSpriteRenderer = GetComponent<SpriteRenderer>();
		}
		Data.Init(scrapCount,this);
		CashedSpriteRenderer.sprite = Resources.Load<Sprite>("Sprites/Scraps/" + Data.ImagePath);
		CashedTransform.localScale = new Vector2(Data.SizeX, Data.SizeY);
		
		//CashedTransform.rotation.ang
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		var collector = other.GetComponent<IScrapCollector>();

		if (collector != null && collector.Collectable)
		{
			collector.CollectScrap(Data);
			Reuse();
		}
	}

	private void OnDisable()
	{
		StopAllCoroutines();
	}

	public Action OnFree { get; set; }
	public IEnumerator WaitForReuse(float delay)
	{
		yield return new WaitForSeconds(delay);
		Reuse();
	}

	public void Reuse()
	{
		gameObject.SetActiveSafe(false);
		OnFree.Invoke();
	}
}
