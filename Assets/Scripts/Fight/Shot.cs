﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using UnityEngine;

public class Shot : MonoBehaviour,IPoolable
{
	public DamageBullet Data;
	//TODO: Добавить не просто не попадание по тому, кто выпустил, а для всех союзных
	public GameObject NotAcceptable;

	private Transform _transform;

	private void OnEnable()
	{
		_transform = transform;
		_reused = false;
		StartCoroutine(WaitForReuse(Data.Range));
	}

	private void FixedUpdate()
	{
		_transform.position+=_transform.up*Time.deltaTime*Data.Speed;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject != NotAcceptable)
		{
			var damagable = other.gameObject.GetComponentInParent<IDamagable>();
			if (damagable != null)
			{
				damagable.GetDamage(Data.DamageType, Data.Damage, this);
				Reuse();
			}
		}
	}

	public Action OnFree { get; set; }
	private bool _reused = false;
	public IEnumerator WaitForReuse(float delay)
	{
		yield return new WaitForSeconds(delay);
		Reuse();
	}
	public void Reuse()
	{
		if (_reused)return;
		gameObject.SetActiveSafe(false);
		OnFree.Invoke();
		_reused = true;
	}
}
