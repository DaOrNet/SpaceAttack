﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Random = UnityEngine.Random;

public class AsteroidScript : MonoBehaviour,IDamagable,IPoolable
{
	private float _rotationSpeed;
	[JsonIgnore][NonSerialized]
	public ConnectionsCollector Collector;
	public Transform SpriteTransform;
	public Transform ContainerTransform;
	public Asteroid Data;

	public bool IsPart;

	public void Init(bool part = false)
	{
		if (Collector==null)
			Collector = new ConnectionsCollector();
		_rotationSpeed = Random.Range(-10f, 10f);
		var angles = SpriteTransform.eulerAngles;
		angles.z = Random.Range(0f, 360f);
		SpriteTransform.eulerAngles = angles;
		if (part)
		{
			IsPart = true;
			Invoke("DisableAsteroid",10f);
		}

		Data.root = this;
		Collector.add = SpriteTransform.OnTriggerEnter2DAsObservable().Subscribe(colliderData => { 
			var damagable = colliderData.gameObject.GetComponent<IDamagable>();

			if (damagable!=null)
			{
				damagable.GetDamage(DamageTypeEnum.AsteroidCollision,Data.CollisionDamage,this);
				Data.Destroyed.Value = true;
				gameObject.SetActiveSafe(false);
			} 
		});

	}

	private void DisableAsteroid()
	{
		gameObject.SetActiveSafe(false);
	}

	private void FixedUpdate()
	{
		var angles = SpriteTransform.eulerAngles;
		angles.z += _rotationSpeed * Time.deltaTime;
		SpriteTransform.eulerAngles = angles;
		if (IsPart)
		{
			ContainerTransform.position+=ContainerTransform.up*Time.deltaTime*1f;
		}
	}

	public void GetDamage(DamageTypeEnum damageType, float damage, MonoBehaviour sender)
	{
		Data.GetDamage(damageType,damage,sender);
	}

	public void ThrowParts(float angle)
	{
		var count = Random.Range(2, 8);
		for (int i = 0; i < count; i++)
		{
			var rand = Random.Range(-30, 30);
			var part = InstanceController.Instance.AsteroidsPools.GetUnusedObject("part");
			part.Init(true);
			part.Data = Data.CloneByJson();
			part.Data.MaxHp = 1;
			part.Data.Init();
			part.Data.root = part;
			part.Data.CollisionDamage = Data.PartDamage;
			part.IsPart = true;
			var euler = part.ContainerTransform.eulerAngles;
			euler.z = angle + rand;
			part.ContainerTransform.eulerAngles = euler;
			part.ContainerTransform.position = ContainerTransform.position;
			part.SetActiveSafe(true);
		}
		
	}
	
	public void GetCriticalDamage(MonoBehaviour sender)
	{
		gameObject.SetActiveSafe(false);
		Data.Destroyed.Value = true;
		if (!IsPart)
		{
			var angle = Mathf.Atan2(ContainerTransform.position.y - sender.transform.position.y,
				            ContainerTransform.position.x - sender.transform.position.x) * Mathf.Rad2Deg;
			ThrowParts(angle-90);
		}
	}

	public float CheckDamagedStatus(MonoBehaviour sender)
	{
		throw new System.NotImplementedException();
	}
	private void OnDisable()
	{
		Collector.DisconnectAll();
		Reuse();
	}
	public Action OnFree { get; set; }
	public IEnumerator WaitForReuse(float delay)
	{
		throw new NotImplementedException();
	}

	public void Reuse()
	{
		OnFree();
	}
}
