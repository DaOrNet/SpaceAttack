﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using UniRx;
using UnityEngine;

public class DataImporter{
  
	public static GameData ParceAllData()
	{
	    var basicsData = new CsvReader(Application.dataPath + "/Raw/Basics.csv");
	    var shipsData = new CsvReader(Application.dataPath + "/Raw/Ships.csv");
	    var weaponsData = new CsvReader(Application.dataPath + "/Raw/Weapons.csv");
	    var asteroidsData = new CsvReader(Application.dataPath + "/Raw/Asteroids.csv");
	    var abilitiesData = new CsvReader(Application.dataPath + "/Raw/Abilities.csv");
	    var scrapsData = new CsvReader(Application.dataPath + "/Raw/Scraps.csv");
	    var shipComponentsData = new CsvReader(Application.dataPath + "/Raw/ShipComponents.csv");
	    var upgradesData = new CsvReader(Application.dataPath + "/Raw/UpgradesData.csv");
		var gd = new GameData();
        
	    ReadBasics(gd,basicsData);
	    gd.ShipComponentsData = ReadShipComponent(shipComponentsData);
	    gd.Abilities = ReadAbilities(abilitiesData);
	    gd.WeaponsData = ReadWeapons(weaponsData);
		gd.AllShips = ReadShips(shipsData,gd);
	    gd.Asteroids = ReadAsteroids(asteroidsData);
	    gd.ScrapsData = ReadScraps(scrapsData);
	    gd.UpgradesData = ReadUpgrades(upgradesData);
		return gd;
	}

    public static void ReadBasics(GameData gd,CsvReader reader)
    {
        gd.MinScrapValue = float.Parse(reader.ReadCellAtRow("MinScrapValue", 1));
        gd.MaxScrapPacks = int.Parse(reader.ReadCellAtRow("MaxScrapPacks", 1));
    }
    public static List<KeyValuePair<string,float>> ReadUpgrades(CsvReader reader)
    {
        var upgrades = new List<KeyValuePair<string, float>>();
        while (reader.NextRow())
        {
            upgrades.Add(new KeyValuePair<string, float>(reader.ReadCell("ID"),float.Parse(reader.ReadCell("Cost"))));
        }
        return upgrades;
    }
    public static List<KeyValuePair<string,ShipComponent>> ReadShipComponent(CsvReader reader)
    {
        var components = new List<KeyValuePair<string, ShipComponent>>();
        while (reader.NextRow())
        {
            var pair = new KeyValuePair<string, ShipComponent>(reader.ReadCell("ID"),
                (ShipComponent) ParceClass(reader));
            pair.Value.NextLevelId = reader.ReadCell("NextLevelId");
            pair.Value.Id = reader.ReadCell("ID");
            components.Add(pair);
        }

        return components;
    }
    public static Ship[] ReadShips(CsvReader reader,GameData gd)
    {
        var ships = new List<Ship>();
        while (reader.NextRow())
        {
            var abilIds = reader.ReadCell("Abilities").Split(',').Select(abil => abil.Trim()).ToArray();
            var componentIds = reader.ReadCell("Components").Split(',').Select(abil => abil.Trim()).ToArray();
            var weaponId = reader.ReadCell("StartWeapon");
            ships.Add(new Ship()
            {
                Id = reader.ReadCell("ID"),
                MaxHp = float.Parse(reader.ReadCell("Hp")),
                MaxShields = float.Parse(reader.ReadCell("Shields")),
                ShieldsRegenValue = float.Parse(reader.ReadCell("ShieldsRegen")),
                HpRegenValue = float.Parse(reader.ReadCell("HpRegen")),
                EnginePower = float.Parse(reader.ReadCell("EnginePower")),
                Mobility = float.Parse(reader.ReadCell("Mobility")),
                ShipPrefabPath = reader.ReadCell("ShipPath"),
                ShipImagePath = reader.ReadCell("ShipImg"),
                Weapons = new List<Weapon>(){gd.WeaponsData.First(pair=>pair.Key==weaponId).Value},
                ShipComponents =  gd.ShipComponentsData.Where(component=>componentIds.Any(id=>id==component.Key)).Select(pair=>pair.Value).ToList(),
                Abilities = gd.Abilities.Where(abil=>abilIds.Any(id=> id==abil.Key)).Select(pair=>pair.Value).ToList(),
                ShipScale = float.Parse(reader.ReadCell("SpriteScale")),
                Worth = float.Parse(reader.ReadCell("Worth")),
            });
        }
        return ships.ToArray();
    }

    public static Asteroid[] ReadAsteroids(CsvReader reader)
    {
        var asteroids = new List<Asteroid>();
        while (reader.NextRow())
        {
            asteroids.Add((Asteroid) ParceClass(reader));
            asteroids[asteroids.Count - 1].Id = reader.ReadCell("ID");
        }
        return asteroids.ToArray();
    }
    
    public static List<KeyValuePair<string,Scrap>> ReadScraps(CsvReader reader)
    {
        var scraps = new List<KeyValuePair<string,Scrap>>();
        while (reader.NextRow())
        {
            var id = reader.ReadCell("ID");
            scraps.Add(new KeyValuePair<string, Scrap>(id,new Scrap()
            {
                Id=id,
                MaxScrapCount = float.Parse(reader.ReadCell("MaxScrapCount")),
                MinScrapCount = float.Parse(reader.ReadCell("MinScrapCount")),
                ImagePath = reader.ReadCell("SpritePath"),
                PrefabPath = reader.ReadCell("PrefabPath"),
                SizeX = float.Parse(reader.ReadCell("SizeX")),
                SizeY = float.Parse(reader.ReadCell("SizeY")),
            }));
            
        }
        return scraps;
    }
    
    public static List<KeyValuePair<string,Ability>> ReadAbilities(CsvReader reader)
    {
        var abilities = new List<KeyValuePair<string,Ability>>();
        while (reader.NextRow())
        {
            var abil = (Ability) ParceClass(reader);
            abil.ReloadTime = float.Parse(reader.ReadCell("ReloadTime"));
            abil.Id = reader.ReadCell("ID");
            abil.IconName = reader.ReadCell("IconName");
            abil.NextLevelId = reader.ReadCell("NextLevelId");
            var keyval= new KeyValuePair<string,Ability>(abil.Id,abil);
            abilities.Add(keyval);
        }
        return abilities;
    }

    public static List<KeyValuePair<string, Weapon>> ReadWeapons(CsvReader reader)
    {
        var weapons = new List<KeyValuePair<string, Weapon>>();
        while (reader.NextRow())
        {
            var pair = new KeyValuePair<string, Weapon>(reader.ReadCell("ID"), (Weapon) ParceClass(reader));
            pair.Value.NextLevelId = reader.ReadCell("NextLevelId");
            pair.Value.Id = reader.ReadCell("ID");
            weapons.Add(pair);
        }
        return weapons;
    }

    public static object ParceClass(CsvReader reader)
    {
        var assembly = Assembly.GetExecutingAssembly();
        var type = assembly.GetTypes()
            .First(t => t.Name == reader.ReadCell("ClassName"));
            
        object result = Activator.CreateInstance(type);
        int i = 1;
        while (reader.HasCell("Attribute"+i))
        {
            string[] keyData = reader.ReadCell("Attribute"+i).Split('=');
            var myFieldInfo = result.GetType().GetField(keyData[0]);
            if (keyData[0]=="")
                break;
            if (myFieldInfo != null)
            {
                var changeTypeResult = ChangeType(keyData[1], myFieldInfo.FieldType);
                myFieldInfo.SetValue(result, changeTypeResult);
            }
            else
            {
                var myPropertyInfo = result.GetType().GetProperty(keyData[0]);
                if (myPropertyInfo==null)
                    throw new Exception("field doesnt exist: " + keyData[0]);
                myPropertyInfo.SetValue(result, ChangeType(keyData[1], myPropertyInfo.PropertyType),null);
            }
            i++;
        }

        return result;
    }

    public static object ChangeType(string data, Type targetType)
    {
        if (targetType.IsEnum)
        {
            return Enum.Parse(targetType, data);
        }

        if (targetType.IsArray)
        {
            
            var arrayData = data.Split(';');
            var eachElement = targetType.GetElementType();
            var changeResult = arrayData.Select(element => Convert.ChangeType(element,eachElement)).ToArray();
            var arrayInstance = Array.CreateInstance(eachElement, arrayData.Length);
            Array.Copy(changeResult, arrayInstance, arrayData.Length);
            return arrayInstance;
        }

        return Convert.ChangeType(data, targetType);
    }
    
    public class CsvReader
    {
        private string[][] _data;
        private int _currentRow=0;
        public CsvReader(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException();
            string[] lines = File.ReadAllLines(path);
            if (lines.Length==0)
                throw new Exception("File has no data");
            List<string[]> full = new List<string[]>();
            foreach (var line in lines)
            {
                var dta = line.Split('\"');
                List<string> data = new List<string>();
                for (int i = 0; i < dta.Length; i++)
                {
                    if ((i+1)%2==0)
                        data.Add(dta[i]);
                    else
                    {                       
                        if (dta[i].EndsWith(",") && i < dta.Length - 1)
                            dta[i] = dta[i].Remove(dta[i].Length - 1);
                        if (dta[i].StartsWith(",") && i > 0)
                            dta[i] = dta[i].Substring(1);  
                        if(dta[i].Length==0)
                            continue;
                        data.AddRange(dta[i].Split(','));
                    }
                }
                full.Add(data.ToArray());
            }
            _data = full.ToArray();
        }
        
        public bool NextRow()
        {
            _currentRow++;
            return _currentRow < _data.Length;
        }
        
        public bool HasCell(string Id)
        {
            for (int i = 0; i < _data[0].Length; i++)
            {
                if (_data[0][i] == Id)
                    return true;
            }
            return false;
        }
        
        public string ReadCell(string Id)
        {
            for (int i = 0; i < _data[0].Length; i++)
            {
                if (_data[0][i] == Id)
                    return _data[_currentRow][i];
            }
            throw new Exception("Id doesn't found : " +Id);
        }
        public string ReadCellAtRow(string rowId,int cellId)
        {
            for (int i = 0; i < _data.Length; i++)
            {
                if (_data[i][0] == rowId)
                    return _data[i][cellId];
            }
            throw new Exception("row doesn't found : " +rowId);
        }
    }
}
